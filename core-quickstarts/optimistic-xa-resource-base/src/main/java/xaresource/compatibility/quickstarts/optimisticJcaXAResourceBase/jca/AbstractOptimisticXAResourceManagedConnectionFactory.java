/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca;

import java.io.PrintWriter;
import java.util.Set;
import java.util.function.Supplier;

import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;
import javax.resource.spi.ConnectionRequestInfo;
import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ManagedConnectionFactory;
import javax.security.auth.Subject;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;

public class AbstractOptimisticXAResourceManagedConnectionFactory<T extends Value<T, C>, C extends Command<T>> implements ManagedConnectionFactory {

	// XXX must be serializable
	private static final long serialVersionUID = 1L;
	
	private final Supplier<ManagedConnection> supplier; 
	
	public AbstractOptimisticXAResourceManagedConnectionFactory(Supplier<ManagedConnection> supplier) {
		this.supplier = supplier;
	}

	@Override
	public Object createConnectionFactory() throws ResourceException {
		return new AbstractOptimisticXAResourceConnectionFactory<T, C>(getDefaultConnectionManager(), this);
	}

	protected ConnectionManager getDefaultConnectionManager() {
		return new AbstractOptimisticXAResourceConnectionManager<T, C>();
	}
	
	@Override
	public Object createConnectionFactory(ConnectionManager cxManager) throws ResourceException {
		return new AbstractOptimisticXAResourceConnectionFactory<T, C>(cxManager, this);
	}

	@Override
	public ManagedConnection createManagedConnection(Subject subject, ConnectionRequestInfo cxRequestInfo) throws ResourceException {
		return supplier.get();
	}

	@Override
	public ManagedConnection matchManagedConnections(@SuppressWarnings("rawtypes") Set connectionSet, Subject subject, ConnectionRequestInfo cxRequestInfo) throws ResourceException {
		return supplier.get();
	}

	@Override
	public PrintWriter getLogWriter() throws ResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLogWriter(PrintWriter out) throws ResourceException {
		// TODO Auto-generated method stub
		
	}

	// XXX must implement the equals and hashcode interfaces

}
