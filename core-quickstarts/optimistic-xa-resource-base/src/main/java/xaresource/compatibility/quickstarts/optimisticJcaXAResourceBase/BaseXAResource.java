/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase;

import javax.transaction.xa.Xid;
import javax.resource.spi.ManagedConnectionFactory;

import xaresource.compatibility.IsolationLevel;

public interface BaseXAResource<T extends Value<T, C>, C extends Command<T>> extends BaseXAResourceRemoteServerSideInterface<T, C>, AutoCloseable {
	
	public void apply(Xid xid, C command);
	public IsolationLevel getIsolationLevel();
	// XXX unclear if this method should be here; in the case that the Resource is an independent long running process
	//     the ManagedConnectionFactory would be a client side entity that only knows how to connect to the Resource.
	public ManagedConnectionFactory getManagedConnectionFactory();
	
}
