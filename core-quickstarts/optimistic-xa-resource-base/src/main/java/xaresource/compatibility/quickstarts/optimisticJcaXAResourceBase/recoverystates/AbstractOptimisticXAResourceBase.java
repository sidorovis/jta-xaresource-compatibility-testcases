/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.recoverystates;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ManagedConnectionFactory;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.IsolationLevel;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResource;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceImmutableState;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext.TransactionContextState;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca.AbstractOptimisticXAResourceManagedConnectionFactory;
import static javax.transaction.xa.XAException.XAER_RMFAIL;

public abstract class AbstractOptimisticXAResourceBase<T extends Value<T, C>, C extends Command<T>> implements BaseXAResource <T, C> {

	private final Logger logger = LoggerFactory.getLogger(AbstractOptimisticXAResourceBase.class);
	
	private static final IsolationLevel isolationLevel = IsolationLevel.Serializable;
	
	private final String id;
	public String getId() {
		return id;
	}
	
	public final BaseXAResource<T, C> ACTIVE = new AbstractOptimisticXAResourceBaseStateActive<T, C>(this);
	public final BaseXAResource<T, C> INCONSISTENT = new AbstractOptimisticXAResourceBaseStateInconsistent<T, C>(this);
	public final BaseXAResource<T, C> RECOVERY = new AbstractOptimsticXAResourceBaseStateRecovery<T, C>(this);
	
	protected final BaseXAResourceImmutableState<T, C> NO_CAS_MATCH = new BaseXAResourceImmutableState<>(null, null, INCONSISTENT);
	
	public final AtomicReference<BaseXAResourceImmutableState<T, C>> state = new AtomicReference<>();
    private final Map<Xid, BaseXAResourceTransactionContext<T, C>> transactionContexts = new ConcurrentHashMap<>();
    protected final AtomicInteger timeout = new AtomicInteger(0);
    
    protected void remoteServerSideXAResourceCleanTransactionResources(Xid xid) {
    	transactionContexts.remove(xid);    	
    }
    
    public boolean isLocked(BaseXAResourceImmutableState<T, C> state) {
    	// currently the only reason for being in locked state is that we are between prepare() and commit()/rollback()
    	return state.preparedValuePlusCommandSequence != null;
    }
    
    protected List<Xid> getXidsOfTransactionContextsInState(TransactionContextState s){
		ArrayList<Xid> xidList = new ArrayList<>();
		for(Xid xid : transactionContexts.keySet()) {
			BaseXAResourceTransactionContext<T, C> tc = transactionContexts.get(xid);
			if(tc.getTransactionContextState().equals(s)) {
				xidList.add(xid);
			}
		}
		return xidList;
    }
    
    public AbstractOptimisticXAResourceBase(String id, T initValue) {
    	BaseXAResourceImmutableState<T, C> s = new BaseXAResourceImmutableState<>(initValue, null, ACTIVE);
    	this.state.set(s);
    	this.id = id;
    	if(null == id || id.trim().equals("")) {
    		String msg = "The id argument must not be null nor empty. It has to be unique among all instances of AbstractLockBasedXAResourceBase!";
    		logger.error(msg);
    		throw new IllegalArgumentException(msg);
    	}
    }
    
	@Override
	public void remoteServerSideXAResourceStart(Xid xid, int flags) throws XAException {
		state.get().recoveryCycleState.remoteServerSideXAResourceStart(xid, flags);		
	}
	
	@Override
	public void remoteServerSideXAResourceEnd(Xid xid, int flags) throws XAException {
		state.get().recoveryCycleState.remoteServerSideXAResourceEnd(xid, flags);
	}
	
	@Override
	public int remoteServerSideXAResourcePrepare(Xid xid) throws XAException {
		return state.get().recoveryCycleState.remoteServerSideXAResourcePrepare(xid);
	}
	
	protected abstract T merge(T base, T update, List<C> commandSequence);
	
	@Override
	public void remoteServerSideXAResourceCommit(Xid xid, boolean onePhase) throws XAException {
		state.get().recoveryCycleState.remoteServerSideXAResourceCommit(xid, onePhase);
	}
	
	@Override
	public void remoteServerSideXAResourceRollback(Xid xid) throws XAException {
		state.get().recoveryCycleState.remoteServerSideXAResourceRollback(xid);
	}
	
	
	@Override
	public boolean isSameRM(XAResource xares) throws XAException {
		// logger.debug("isSameRM()");
		logger.error("isSameRM(): this should never be called; this call sould be handled in AbstractOptimisticXAResourceManagedConnection.LocalXAResource.isSameRM()");
		throw new XAException(XAER_RMFAIL);
		/*
        return (xares instanceof AbstractOptimisticXAResourceBase 
        		&& 
        		this.id.equals(((AbstractOptimisticXAResourceManagedConnection.LocalXAResource)xares).this.id)
        		);
		 */
	}

	@Override
	public void forget(Xid xid) throws XAException {
		state.get().recoveryCycleState.forget(xid);
	}

	@Override
	public Xid[] recover(int flag) throws XAException {
		return state.get().recoveryCycleState.recover(flag);
	}
	
	@Override
	public int getTransactionTimeout() throws XAException {
		return state.get().recoveryCycleState.getTransactionTimeout();
	}

	@Override
	public boolean setTransactionTimeout(int seconds) throws XAException {
		return state.get().recoveryCycleState.setTransactionTimeout(seconds);
	}

	@Override
	public void apply(Xid xid, C command) {
		state.get().recoveryCycleState.apply(xid, command);
	}

    public T getImmutableValue(Xid xid) {
    	if(null == xid) return null;
    	return (T)this.getTransactionContext(xid).getValue();
    }
    
	public BaseXAResourceTransactionContext<T, C> getTransactionContext(Xid xid) {
		if(null == xid) return null;// if there is none
		BaseXAResourceTransactionContext<T, C> tc = transactionContexts.get(xid);
		if(null == tc) return null;
		return tc;		
	}

	public void setTransactionContext(Xid xid, BaseXAResourceTransactionContext<T, C> tc) {
		if(null == xid) {
			throw new IllegalStateException("You tried to call setTransactionContext() with a null xid!");
		}
		if(null == tc) {
			throw new IllegalStateException("You tried to call setTransactionContext() with a null transaction context!");			
		}
		transactionContexts.put(xid, tc);
	}
	
	@Override
	public void close() throws Exception {
		state.get().recoveryCycleState.close();
	}
	
	public IsolationLevel getIsolationLevel() {
		return isolationLevel;
	}
	
	public abstract Supplier<ManagedConnection> getManagedConnectionSupplier();

	public ManagedConnectionFactory getManagedConnectionFactory() {
		return new AbstractOptimisticXAResourceManagedConnectionFactory<T, C>(getManagedConnectionSupplier());
	}
	
	public void switchToInconsistentState(BaseXAResourceImmutableState<T, C> state) {
		if(null == state)
			state = this.state.get();
		BaseXAResourceImmutableState<T, C> inconsistent = new BaseXAResourceImmutableState<>(state.value, state.preparedValuePlusCommandSequence, INCONSISTENT);
		this.state.set(inconsistent);		
	}
	
	
}
