/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca;

import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;
import javax.resource.spi.ConnectionRequestInfo;
import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ManagedConnectionFactory;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;

public class AbstractOptimisticXAResourceConnectionManager<T extends Value<T, C>, C extends Command<T>> implements ConnectionManager {

	private static final long serialVersionUID = 1L;

	@Override
	public Object allocateConnection(ManagedConnectionFactory mcf, ConnectionRequestInfo cxRequestInfo) throws ResourceException {
		ManagedConnection mc = mcf.createManagedConnection(null, cxRequestInfo);
		return mc.getConnection(null, cxRequestInfo);
	}

}
