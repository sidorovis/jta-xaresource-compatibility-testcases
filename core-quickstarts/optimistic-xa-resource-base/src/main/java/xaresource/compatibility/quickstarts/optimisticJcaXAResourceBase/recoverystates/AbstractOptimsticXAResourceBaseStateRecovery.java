/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.recoverystates;

import static javax.transaction.xa.XAException.XAER_RMFAIL;

import javax.resource.spi.ManagedConnectionFactory;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import xaresource.compatibility.IsolationLevel;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResource;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceImmutableState;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;

public class AbstractOptimsticXAResourceBaseStateRecovery<T extends Value<T, C>, C extends Command<T>> implements BaseXAResource<T, C> {
	
	public final AbstractOptimisticXAResourceBase<T, C> base;
	
	public AbstractOptimsticXAResourceBaseStateRecovery(AbstractOptimisticXAResourceBase<T, C> base) {
		this.base = base;
	}

	@Override
	public void remoteServerSideXAResourceStart(Xid xid, int flags) throws XAException {
		throw new XAException(XAER_RMFAIL);
	}
	
	@Override
	public void remoteServerSideXAResourceEnd(Xid xid, int flags) throws XAException {
		throw new XAException(XAER_RMFAIL);
	}
	
	@Override
	public int remoteServerSideXAResourcePrepare(Xid xid) throws XAException {
		throw new XAException(XAER_RMFAIL);
	}
	
	@Override
	public void remoteServerSideXAResourceCommit(Xid xid, boolean onePhase) throws XAException {
		// XXX to be implemented
	}
	
	@Override
	public void remoteServerSideXAResourceRollback(Xid xid) throws XAException {
		// XXX to be implemented
	}
	
	@Override
	public boolean isSameRM(XAResource xares) throws XAException {
		// XXX to be implemented
		return false;
	}

	@Override
	public void forget(Xid xid) throws XAException {
		// XXX to be implemented
	}

	@Override
	public Xid[] recover(int flag) throws XAException {
		// XXX to be implemented
		return new Xid[0];
	}
	
	@Override
	public int getTransactionTimeout() throws XAException {
		return base.timeout.get();
	}

	@Override
	public boolean setTransactionTimeout(int seconds) throws XAException {
		base.timeout.set(seconds);
		return true;
	}
	
	@Override
	public void apply(Xid xid, C t) {
		throw new IllegalStateException();
	}

	@Override
	public void close() throws Exception {
		throw new IllegalStateException();
	}
	
	@Override
	public IsolationLevel getIsolationLevel() {
		return base.getIsolationLevel();
	}

	public ManagedConnectionFactory getManagedConnectionFactory() {
		return base.getManagedConnectionFactory();
	}
	
	@Override
	public BaseXAResourceTransactionContext<T, C> getTransactionContext(Xid xid) {
		throw new IllegalStateException();
	}

	@Override
	public void setTransactionContext(Xid xid, BaseXAResourceTransactionContext<T, C> tc) {
		throw new IllegalStateException();
	}

	@Override
	public T getImmutableValue(Xid xid) {
		throw new IllegalStateException();
	}

	@Override
	public boolean isLocked(BaseXAResourceImmutableState<T, C> state) {
		throw new IllegalStateException();
	}
}
