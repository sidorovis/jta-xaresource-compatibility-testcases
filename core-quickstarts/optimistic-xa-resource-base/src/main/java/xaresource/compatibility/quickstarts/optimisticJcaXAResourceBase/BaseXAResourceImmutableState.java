/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase;

import net.jcip.annotations.Immutable;

@Immutable
public class BaseXAResourceImmutableState<T extends Value<T, C>, C extends Command<T>> {
	
	public final T value;
	public final ValuePlusCommandSequence<T, C> preparedValuePlusCommandSequence;
	public final BaseXAResource<T, C> recoveryCycleState;
	
	public BaseXAResourceImmutableState(T value, ValuePlusCommandSequence<T, C> preparedValuePlusCommandSequence, BaseXAResource<T, C> recoveryCycleState) {
		this.value = value;
		this.preparedValuePlusCommandSequence = preparedValuePlusCommandSequence;
		this.recoveryCycleState = recoveryCycleState;
	}
	

}
