/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.recoverystates;

import static javax.transaction.xa.XAException.XAER_RMFAIL;
import static xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext.TransactionContextState.*;
import static javax.transaction.xa.XAResource.*;

import java.util.List;

import javax.resource.spi.ManagedConnectionFactory;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.IsolationLevel;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResource;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceImmutableState;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.ValuePlusCommandSequence;

public class AbstractOptimisticXAResourceBaseStateActive<T extends Value<T, C>, C extends Command<T>> implements BaseXAResource<T, C> {
	
	private final Logger logger = LoggerFactory.getLogger(AbstractOptimisticXAResourceBaseStateActive.class);
	
	public final AbstractOptimisticXAResourceBase<T, C> base;
	
	public AbstractOptimisticXAResourceBaseStateActive(AbstractOptimisticXAResourceBase<T, C> base) {
		this.base = base;
	}

	@Override
	public void remoteServerSideXAResourceStart(Xid xid, int flags) throws XAException {
		logger.debug("remoteServerSideXAResourceStart()");
        if (logger.isDebugEnabled()) {
        	logger.debug(new StringBuffer(128)
        		.append(this + ": ")
        		.append("Thread ").append(Thread.currentThread())
        		.append(flags == TMNOFLAGS ? " starts" : flags == TMJOIN ? " joins" : " resumes")
        		.append(" work on behalf of transaction branch ")
        		.append(xid).toString());
        }
        
        BaseXAResourceTransactionContext<T, C> tc;
        switch (flags) {
            // a new transaction
            case TMNOFLAGS :
            default :
            	if(base.getTransactionContext(xid) !=  null) {
            		String msg = "start(TMNOFLAGS): Transaction context for xid: " + xid + " already exists!";
                	logger.error(msg);
					throw new XAException(msg);
            	}            		
                tc = new BaseXAResourceTransactionContext<>(xid, base.state.get());
                break;
            case TMJOIN :
                tc = base.getTransactionContext(xid);
                if (tc == null) {
                    throw new XAException(XAException.XAER_NOTA);
                }
                if(tc.isSuspended())
                	throw new XAException(XAException.XAER_PROTO);
                tc.setTransactionContextState(ACTIVE);
                break;
            case TMRESUME :
                tc = base.getTransactionContext(xid);
                if (tc == null) {
                    throw new XAException(XAException.XAER_NOTA);
                }
                if(!tc.isSuspended())
                	throw new XAException(XAException.XAER_PROTO);
                tc.setTransactionContextState(ACTIVE);
                break;
        }
        base.setTransactionContext(xid, tc);
	}
	
	@Override
	public void remoteServerSideXAResourceEnd(Xid xid, int flags) throws XAException {
		logger.debug("remoteServerSideXAResourceEnd()");

		BaseXAResourceTransactionContext<T, C> tc = base.getTransactionContext(xid); // checks that the transaction is active
		if(null == tc || tc.getTransactionContextState() != ACTIVE)
			throw new XAException("Transaction Context is not active!");
		
        if (logger.isDebugEnabled()) {
        	logger.debug(new StringBuffer(128)
        		.append(this + ": ")
	            .append("Thread ").append(Thread.currentThread())
	            .append(flags == TMSUSPEND ? " suspends" : flags == TMFAIL ? " fails" : " ends")
	            .append(" work on behalf of transaction branch ")
	            .append(xid).toString());
        }

        switch (flags) {
            case TMSUSPEND :
            	tc.setTransactionContextState(SUSPENDED);
                break;
            case TMFAIL :
            	tc.setTransactionContextState(MARKED_ROLLBACK);
                break;
            case TMSUCCESS :
            	tc.setTransactionContextState(ENDED);
                break;
        }
	}
	
	@Override
	public int remoteServerSideXAResourcePrepare(Xid xid) throws XAException {
		logger.debug("remoteServerSideXAResourcePrepare()");
		BaseXAResourceTransactionContext<T, C> tc = base.getTransactionContext(xid);
		if(null == tc)
			throw new XAException(XAException.XAER_NOTA);

        if (logger.isDebugEnabled()) {
        	logger.debug(this + ": " + "Internal Preparing transaction branch " + xid);
        }

        if (tc.getTransactionContextState() == MARKED_ROLLBACK) {
            throw new XAException(XAException.XA_RBROLLBACK);
        }
        	
        int result = (tc.getValue().isReadOnly() ? XA_RDONLY : XA_OK);
        
        BaseXAResourceImmutableState<T, C> state = this.base.state.get();
        if(state.preparedValuePlusCommandSequence != null)
    		throw new XAException(XAException.XA_RBROLLBACK); // some other transaction is currently in prepared state
        
        BaseXAResourceImmutableState<T, C> newState;
    	BaseXAResourceImmutableState<T, C> transactionStartState = tc.getBaseState();
        if(result == XA_RDONLY) {
        	base.remoteServerSideXAResourceCleanTransactionResources(xid);
        	
        	// some other transaction is currently in prepared state or other action/transaction updated the counter before me.
        	// it is only necessary to throw and exception here if you want to have full serializable transactions and not only read-committed semantics 
            if(this.base.getIsolationLevel() == IsolationLevel.Serializable && this.base.state.get() != transactionStartState)
        		throw new XAException(XAException.XA_RBROLLBACK);         	
        } else {
            ValuePlusCommandSequence<T, C> vpcs = tc.getValue().getValuePlusCommandSequence(xid);
            newState = new BaseXAResourceImmutableState<>(state.value, vpcs, this);
            if(!this.base.state.compareAndSet(transactionStartState, newState)) {
            	base.remoteServerSideXAResourceCleanTransactionResources(xid);
        		throw new XAException(XAException.XA_RBROLLBACK); // some other transaction is currently in prepared state or other action/transaction updated the counter before me.
            }
            tc.setTransactionContextState(PREPARED);
        }
		return result;
	}
	
	protected boolean isInValidPreparedState(BaseXAResourceImmutableState<T, C> state, BaseXAResourceTransactionContext<T, C> tc) throws XAException {
		if(null == state.preparedValuePlusCommandSequence)
			return false;
		
		// the prepared value reflects the value of the transaction context associated with the xid
		// "The value associated with the preparedValuePlusCommandSequence does not match the transaction context value!"
		if(!state.preparedValuePlusCommandSequence.getXid().equals(tc.getXid()))
			return false;
		
		return true;
	}
	
	@Override
	public void remoteServerSideXAResourceCommit(Xid xid, boolean onePhase) throws XAException {
		logger.debug("remoteServerSideXAResourceCommit()");
        if (logger.isDebugEnabled()) {
        	logger.debug(this + ": " + "Internally committing transaction branch '" + xid + "' onePhase: " + onePhase);
        }
		
		BaseXAResourceTransactionContext<T, C> tc = base.getTransactionContext(xid);
		if(null == tc)
			throw new XAException(XAException.XAER_NOTA);

        if (tc.getTransactionContextState() == MARKED_ROLLBACK) {
            throw new XAException(XAException.XA_RBROLLBACK);
        }
        
        if (tc.getTransactionContextState() != PREPARED) {
            if (onePhase) {
                if (logger.isDebugEnabled()) {
                	logger.debug(this + ": " + "One Phase Committing transaction branch " + xid);
                }
            	try {
            		int prepare = remoteServerSideXAResourcePrepare(xid);
            		if(prepare == XA_RDONLY) {
            			logger.debug("One phase transaction with a XA_RDONLY prepare statement can be short circuited.");
            	    	this.base.remoteServerSideXAResourceCleanTransactionResources(xid);
            			return;
            		}
            	} catch(XAException e) {
            		logger.debug("The onePhase prepare threw an XAException!");
                	// XXX should I switch to inconsitent state here?
                	// switchToInconsistentState(state);
            		throw e;
            	}
            } else {
                throw new XAException(XAException.XAER_PROTO);
            }
        }
        
        BaseXAResourceImmutableState<T, C> state = this.base.state.get();
        if(!isInValidPreparedState(state, tc)) {
        	base.switchToInconsistentState(state);
        	throw new XAException("This should never happend!");
        }

		T start = state.preparedValuePlusCommandSequence.getBaseValue();
		T end = state.preparedValuePlusCommandSequence.getFinalValue();
		List<C> commandSequence = state.preparedValuePlusCommandSequence.getCommandSequence();
		
		T newValue = base.merge(start, end, commandSequence);
		
		BaseXAResourceImmutableState<T, C> newState = new BaseXAResourceImmutableState<>(newValue, null, this);
		// XXX think about if I should switch to INCONSISTENT state in cases like this one: I guess yes!
        if(!this.base.state.compareAndSet(state, newState)) {
        	base.switchToInconsistentState(state);
    		throw new XAException("This should never happen!"); // some other transaction is currently in prepared state or other action/transaction updated the counter before me.
        }
        
        // only clean up if we successfully finish the commit, otherwise this data may be needed for recovery
    	this.base.remoteServerSideXAResourceCleanTransactionResources(xid);
	}
	
	@Override
	public void remoteServerSideXAResourceRollback(Xid xid) throws XAException {
		logger.debug("remoteServerSideXAResourceRollback()");
        if (logger.isDebugEnabled()) {
        	logger.debug(this + ": " + "Internally rolling back transaction branch '" + xid + "'");
        }
        
		BaseXAResourceTransactionContext<T, C> tc = base.getTransactionContext(xid);
		if(null == tc)
			throw new XAException(XAException.XAER_NOTA);
		
        BaseXAResourceImmutableState<T, C> state = this.base.state.get();
        /* a rollback should only happen if the transaction context is in state ENDED or PREPARED
         * it can be PREPARED but not necessarily a valid prepared (isInValidPreparedState()), 
         * if we have to XA resources and this transaction context is in PREPARED from another transaction 
         * but this transaction context is rolled back due to a failing prepare of the other XA resource
         * 
         */
        if(!(tc.getTransactionContextState().equals(MARKED_ROLLBACK) || tc.getTransactionContextState().equals(ENDED) || tc.getTransactionContextState().equals(PREPARED))) {
        	logger.error("The transaction context is in state: '" + tc.getTransactionContextState() + "'");
        	base.switchToInconsistentState(state);
        	throw new XAException("This should never happend!");
        }

        if(isInValidPreparedState(state, tc)) {
        	// if we are in a valid prepared state the we need to clean it up.
        	// otherwise this resource is in a prepared state of another transaction.
    		BaseXAResourceImmutableState<T, C> newState = new BaseXAResourceImmutableState<>(state.value, null, this);
            if(!this.base.state.compareAndSet(state, newState)) {
            	base.switchToInconsistentState(state);
        		throw new XAException("This should never happen!"); // some other transaction is currently in prepared state or other action/transaction updated the counter before me.
            }        	
        }
		
    	this.base.remoteServerSideXAResourceCleanTransactionResources(xid);
	}
	
	@Override
	public boolean isSameRM(XAResource xares) throws XAException {
		logger.error("isSameRM(): this should never be called; this call sould be handled in AbstractLockBasedXAResourceBase.");
		throw new XAException(XAER_RMFAIL);
	}

	@Override
	public void forget(Xid xid) throws XAException {
		// XXX to be implemented
	}

	@Override
	public Xid[] recover(int flag) throws XAException {
		logger.debug("recover()");
		List<Xid> xidList = base.getXidsOfTransactionContextsInState(PREPARED);
		logger.debug("recover(): " + xidList);
		return xidList.toArray(new Xid[xidList.size()]);
	}
	
	@Override
	public int getTransactionTimeout() throws XAException {
		return base.timeout.get();
	}

	@Override
	public boolean setTransactionTimeout(int seconds) throws XAException {
		base.timeout.set(seconds);
		return true;
	}
	
	@Override
	public void apply(Xid xid, C command) {
		BaseXAResourceTransactionContext<T, C> tc = base.getTransactionContext(xid);
		if(null == tc) {
			BaseXAResourceImmutableState<T, C> state = null;
			BaseXAResourceImmutableState<T, C> newState = null;
			int count = 0;
			do {
				count++;
				// XXX perhaps some sort of wait/notify solution would be better here.
				if(count % 100 == 0) try {
					Thread.sleep(1);
				} catch (InterruptedException e) {}
				state = base.state.get();
				if(base.isLocked(state))
					state = base.NO_CAS_MATCH; // this will cause the compareAndSet operation below to fail so that we wait here until the XAResource has left the prepared state.
				
				T current = state.value;
				T update = command.apply(current);
				
				// XXX I still need to strip off the history from the update:
				newState = new BaseXAResourceImmutableState<>(update, null, this); 
			} while(!base.state.compareAndSet(state, newState));
		} else {
			T current = (T) tc.getValue();
			T update = command.apply(current);
			tc.setValue(update);
		}		
	}

	@Override
	public void close() throws Exception {
		// up to now no need to close; will come with command log
	}

	@Override
	public IsolationLevel getIsolationLevel() {
		return base.getIsolationLevel();
	}
	
	public ManagedConnectionFactory getManagedConnectionFactory() {
		return base.getManagedConnectionFactory();
	}
	
	@Override
	public BaseXAResourceTransactionContext<T, C> getTransactionContext(Xid xid) {
		return base.getTransactionContext(xid);
	}

	@Override
	public void setTransactionContext(Xid xid, BaseXAResourceTransactionContext<T, C> tc) {
		base.setTransactionContext(xid, tc);
	}

	@Override
	public T getImmutableValue(Xid xid) {
		return base.getImmutableValue(xid);
	}

	@Override
	public boolean isLocked(BaseXAResourceImmutableState<T, C> state) {
		return base.isLocked(state);
	}
	
}
