/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase;

import static xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext.TransactionContextState.*;

import javax.transaction.Status;
import javax.transaction.xa.Xid;

/*
 * If this class is used from the same JVM where the user application is running then it will be used single threaded and synchronization would not be necessary.
 * The mechanism would be that you have a bijective association between a thread and its Xid, there would be never the case that two different threads have the same Xid 
 * as the top of the transaction stack and the getActiveTransactionStack() would return the single Xid that belongs to this thread.
 * 
 * But if this class is used in a remoting set-up then you cannot rely on a one-to-one relationship between thread and xid.
 */
public class BaseXAResourceTransactionContext<T extends Value<T, C>, C extends Command<T>> {
	protected final Xid xid;
	protected final BaseXAResourceImmutableState<T, C> baseState;
	protected BaseXAResourceTransactionContext.TransactionContextState state = BaseXAResourceTransactionContext.TransactionContextState.ACTIVE;
	protected T value;
	
	public BaseXAResourceTransactionContext(Xid xid, BaseXAResourceImmutableState<T, C> baseState) {
		this.xid = xid;
		this.baseState = baseState;
		this.value = baseState.value;
	}
	
	public Xid getXid() {
		return xid;
	}
	
	public TransactionContextState getTransactionContextState() {
		return state;
	}
	
	public BaseXAResourceImmutableState<T, C> getBaseState() {
		return this.baseState;
	}
	
	public synchronized boolean isSuspended() {
		return state == SUSPENDED;
	}

	public synchronized boolean isActive() {
		return state == ACTIVE;
	}
	
	public synchronized void setTransactionContextState(TransactionContextState tcs) {
		this.state = tcs;
	}
	
	public synchronized T getValue() {
		return this.value;
	}

	public synchronized void setValue(T value) {
		this.value = value;
	}
	
	public static enum TransactionContextState {
		SUSPENDED(10000+Status.STATUS_NO_TRANSACTION), ACTIVE(Status.STATUS_ACTIVE), MARKED_ROLLBACK(Status.STATUS_MARKED_ROLLBACK), ENDED(20000+Status.STATUS_NO_TRANSACTION), PREPARED(Status.STATUS_PREPARED);
		private final int value;
		private TransactionContextState(int value) {
			this.value = value;
		}
		public int getValue() {
	        return value;
	    }
	}
}
