/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca;

import static xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext.TransactionContextState.PREPARED;

import java.io.PrintWriter;
import java.util.Stack;

import javax.resource.ResourceException;
import javax.resource.spi.ConnectionEventListener;
import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ManagedConnectionMetaData;
import javax.transaction.Status;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import net.jcip.annotations.NotThreadSafe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceImmutableState;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.BaseXAResourceTransactionContext.TransactionContextState;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.recoverystates.AbstractOptimisticXAResourceBase;

// It is important to understand that a connection is inherently an entity that is meant to be used in a single threaded way! 
@NotThreadSafe
public abstract class AbstractOptimisticXAResourceManagedConnection<T extends Value<T, C>, C extends Command<T>> implements ManagedConnection {

	private final Logger logger = LoggerFactory.getLogger(AbstractOptimisticXAResourceManagedConnection.class);
	
	private final AbstractOptimisticXAResourceBase<T, C> baseXAResource;
	
	private Stack<Xid> activeTransactionStack = new Stack<Xid>();
	
	protected AbstractOptimisticXAResourceManagedConnection(AbstractOptimisticXAResourceBase<T, C> baseXAResource) {
		this.baseXAResource = baseXAResource;
	}
	
	public AbstractOptimisticXAResourceBase<T, C> getBaseResource() {
		return baseXAResource;
	}

	/*
	@Override
	public Object getConnection(Subject subject, ConnectionRequestInfo cxRequestInfo) throws ResourceException {
		return new LocalConnection();
	}
	*/

	@Override
	public void destroy() throws ResourceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cleanup() throws ResourceException {
		// TODO Auto-generated method stub
		
	}

	/*
	@Override
	public void associateConnection(Object connection) throws ResourceException {
		// TODO Auto-generated method stub
		
	}
	*/

	@Override
	public void addConnectionEventListener(ConnectionEventListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeConnectionEventListener(ConnectionEventListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public XAResource getXAResource() throws ResourceException {
		// TODO Auto-generated method stub
		return new LocalXAResource();
	}

	@Override
	public javax.resource.spi.LocalTransaction getLocalTransaction() throws ResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ManagedConnectionMetaData getMetaData() throws ResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PrintWriter getLogWriter() throws ResourceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLogWriter(PrintWriter out) throws ResourceException {
		// TODO Auto-generated method stub
		
	}

	public class LocalXAResource implements XAResource {
		
		@Override
		public void start(Xid xid, int flags) throws XAException {
			logger.debug("start()");
	        if (isTransactionActive())
	            throw new XAException(XAException.XAER_INVAL);
			baseXAResource.state.get().recoveryCycleState.remoteServerSideXAResourceStart(xid, flags);
	        getActiveTransactionStack().push(xid);
		}
		
		@Override
		public void end(Xid xid, int flags) throws XAException {
			logger.debug("end()");
			if(!verifyActiveTransactionContext(xid))
	            throw new XAException(XAException.XAER_NOTA);
			baseXAResource.state.get().recoveryCycleState.remoteServerSideXAResourceEnd(xid, flags);
	        getActiveTransactionStack().pop();
		}

		@Override
		public int prepare(Xid xid) throws XAException {
			logger.debug("prepare()");
	        if (logger.isDebugEnabled()) {
	        	logger.debug(this + ": " + "Preparing transaction branch " + xid);
	        }

	        int result = Status.STATUS_UNKNOWN;
	        if(verifyActiveTransactionContext(xid))  {
	        	// this should not happen, not even if we are within a commit of a one phase transaction 
	        	throw new IllegalStateException("This still needs to be thought through.");
	        }
	    	
	        getActiveTransactionStack().push(xid);
	        try {
	        	result = baseXAResource.state.get().recoveryCycleState.remoteServerSideXAResourcePrepare(xid);
	        } finally {
	            /*
	             * lock and unlock are not necessary, because we're using the compareAndSet approach.
	             * also all of the undo logic that we had before for undoing part of the state changes is gone
	             * because state changes happen in an all or nothing fashion
	             */
	            getActiveTransactionStack().pop();
	        }        	
	        if (result == XA_RDONLY) {
	            if (logger.isDebugEnabled()) {
	            	logger.debug(this + ": " + "Read Only Prepare for transaction branch " + xid);
	            }
	        }
	        
	        return result;
		}

		@Override
		public void commit(Xid xid, boolean onePhase) throws XAException {
			logger.debug("commit()");
	        if (logger.isDebugEnabled()) {
	        	logger.debug(this + ": " + "Committing transaction branch '" + xid + "' onePhase: " + onePhase);
	        }
	        
	        
	        BaseXAResourceImmutableState<T, C> state = baseXAResource.state.get();
	        
	        if(!onePhase && !state.preparedValuePlusCommandSequence.getXid().equals(xid)) {
	        	logger.error("A commit is trying to commit for another transaction id: '" + xid + "' than the transaction id of the prepared transaction: '" + state.preparedValuePlusCommandSequence.getXid() + "'!! The prepare method for this xid should have already failed!");
	        	throw new XAException("This should never happend!");
	        }
	        
	        if(verifyActiveTransactionContext(xid))  {
	        	throw new IllegalStateException("This still needs to be thought through.");
	        }
	        
	        getActiveTransactionStack().push(xid);
	        try {
	            
	            if(!onePhase && !verifyThatActiveTransactionContextMatchesPreparedTransaction(state)) {
	            	logger.error("The transaction context is in state: '" + getActiveTransactionContext().getTransactionContextState() + "'");
	            	baseXAResource.switchToInconsistentState(state);
	            	throw new XAException("This should never happend!");
	            }

	            baseXAResource.state.get().recoveryCycleState.remoteServerSideXAResourceCommit(xid, onePhase);
	        } finally {
	        	getActiveTransactionStack().pop();
	        }
		}

		@Override
		public void rollback(Xid xid) throws XAException {
			logger.debug("rollback()");
	        if (logger.isDebugEnabled()) {
	        	logger.debug(this + ": " + "Rolling back transaction branch '" + xid + "'");
	        }
		
	        //AbstractLockBasedXAResourceImmutableState<T, C> state = this.base.state.get();

	        if(verifyActiveTransactionContext(xid))  {
	        	throw new IllegalStateException("This still needs to be thought through.");
	        }
	        
	        getActiveTransactionStack().push(xid);
	        try {
	        	/*
	        	 * In the same way as there is a onePhase commit a rollback can happen without first going through the prepared sate.
	        	 * A rollback can happen after the end() or after prepare() state.
	        	 */
	        	/* This is wrong:
	            if(!verifyThatActiveTransactionContextMatchesPreparedTransaction(state)) {
	            	switchToInconsistentState(state);
	            	throw new XAException("This should never happend!");
	            }
	            */
	            
	        	baseXAResource.state.get().recoveryCycleState.remoteServerSideXAResourceRollback(xid);
	        } finally {
	        	getActiveTransactionStack().pop();        	
	        }        
		}

		@SuppressWarnings("rawtypes")
		public boolean isSameRM(XAResource xares) throws XAException {
			logger.debug("isSameRM()");
			
	        return (xares instanceof AbstractOptimisticXAResourceManagedConnection.LocalXAResource 
	        		&& 
	        		getId().equals(((AbstractOptimisticXAResourceManagedConnection.LocalXAResource)xares).getId())
	        		);
		}
		
		protected String getId() {
			return baseXAResource.getId();
		}
		
		@Override
		public void forget(Xid xid) throws XAException {
			baseXAResource.state.get().recoveryCycleState.forget(xid);
		}

		@Override
		public Xid[] recover(int flag) throws XAException {
			return baseXAResource.state.get().recoveryCycleState.recover(flag);
		}
		
		@Override
		public int getTransactionTimeout() throws XAException {
			return baseXAResource.state.get().recoveryCycleState.getTransactionTimeout();
		}

		@Override
		public boolean setTransactionTimeout(int seconds) throws XAException {
			return baseXAResource.state.get().recoveryCycleState.setTransactionTimeout(seconds);
		}
		
		
	}

    protected Stack<Xid> getActiveTransactionStack() {
    	return activeTransactionStack;
    }
    
	public boolean isTransactionActive() {
		return !(getActiveTransactionStack().size() == 0); 
	}    

	protected Xid peekActiveTransactionStack() {
		if(getActiveTransactionStack().empty())
			return null;
		return getActiveTransactionStack().peek();
	}
	
	protected boolean verifyActiveTransactionContext(Xid xid) {
		Xid activeXid = peekActiveTransactionStack();
		if(null == activeXid || !activeXid.equals(xid))
			return false;
		else
			return true;
	}
	
	protected BaseXAResourceTransactionContext<T, C> getActiveTransactionContext() {
		Xid xid = peekActiveTransactionStack(); // get for the current thread the active xid
		return baseXAResource.state.get().recoveryCycleState.getTransactionContext(xid);
	}
	
	protected BaseXAResourceTransactionContext<T, C> getActiveTransactionContext(TransactionContextState stateToCompareTo) {
		BaseXAResourceTransactionContext<T, C> tc = getActiveTransactionContext();
		if(null == tc || tc.getTransactionContextState() != stateToCompareTo) return null;
		return tc;
	}
    
	// this method should always be called on the client side and never on the server side if this happens in a client/server set-up
	// the client side then translates into a call where the apply is associated to a Xid.
	public void apply(C command) {
		Xid xid = peekActiveTransactionStack();
		baseXAResource.state.get().recoveryCycleState.apply(xid, command);
	}	
	
    protected T getImmutableValue() {
    	if(this.isTransactionActive()) {
    		Xid xid = this.peekActiveTransactionStack();
    		return baseXAResource.state.get().recoveryCycleState.getImmutableValue(xid);
    	} else {
        	return baseXAResource.state.get().value;
    	}
    }
    
	public int getStatus() {
		BaseXAResourceImmutableState<T, C> state = baseXAResource.state.get();
		
		if(null != state.preparedValuePlusCommandSequence || baseXAResource.state.get().recoveryCycleState.isLocked(state))
			return Status.STATUS_PREPARED;
		
		BaseXAResourceTransactionContext<T, C> tc = getActiveTransactionContext();
		if(null == tc)
			return Status.STATUS_NO_TRANSACTION;
		
		return tc.getTransactionContextState().getValue();
	}
	
	protected boolean verifyThatActiveTransactionContextMatchesPreparedTransaction(BaseXAResourceImmutableState<T, C> state) throws XAException {
		// the active transaction is in prepared state
		if(null == getActiveTransactionContext(PREPARED))
			return false;
		// the value of the active transaction corresponds to the prepared value
        if(getActiveTransactionContext(PREPARED).getValue().equals(state.preparedValuePlusCommandSequence.getFinalValue()))
        	return true;
        else
        	return false;
	}
	
	
  

	
	
}
