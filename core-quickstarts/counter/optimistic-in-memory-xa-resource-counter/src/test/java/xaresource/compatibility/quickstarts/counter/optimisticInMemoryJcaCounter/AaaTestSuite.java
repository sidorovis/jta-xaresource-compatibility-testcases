/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import xaresource.compatibility.testcases.MyXidTest;


/*
 * mvn test -DskipTests=false -Dgroups="xaresource.TestCategorySpeedFast"
 * mvn test -DskipTests=false -Dgroups="xaresource.TestCategorySpeedFast, xaresource.TestCategorySpeedSlow"
 */

@RunWith(Suite.class)
@SuiteClasses({
	MyXidTest.class
	,SingleXAResourceSinglethreadedTest.class 
	,SingleXAResourceSinglethreadedAdvancedTest.class
	,TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedTest.class
	,TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedRecoveryTest.class
	,TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTest.class
	,SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest.class
	,TwoXAResourcesWithNarayanaTransactionManagerMultiThreadedInvariantTest.class
	})
public class AaaTestSuite {
	
	/*
	 * The following references are only here so that you can use your IDE features to quickly navigate to those tests
	 */
	
	// you can ignore this.
	public static final MyXidTest x010 = null;
	
	/*
	 * When you start to develop an implementation of a class that fulfills the XAResource interface and protocol then start here.
	 */
	public static final SingleXAResourceSinglethreadedTest a010 = null;
	// these tests may not work with all implementations of XA resources:
	public static final SingleXAResourceSinglethreadedAdvancedTest a015 = null;
	
	/*
	 * The next level verifies that two independent XAResources and the Narayana transaction manager work well together
	 * in a single threaded set-up.
	 */
	public static final TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedTest a020 = null;

	public static final TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedRecoveryTest a025 = null;
	
	/*
	 * This test does not do more than the previous test, but it shows how to interact with the Spring Declarative Transaction mechanisms.
	 * This is more an example of how to use an XAResource together with Spring Declarative Transactions than a test case that helps you
	 * to verify the correctness of your implementation. Therefore it is not included in the suite above.
	 */
	public static final TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTest a030 = null;
	
	/*
	 * This is the first test where two threads operate on a single XAResource.
	 * This test performs an exhaustive combinatorial interleave test of the steps between the two threads on this single resource.
	 */
	public static final SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest a040 = null;
	
	/*
	 * This is a test where many/multiple threads operate in a random interleave sequence on two independent XAResources, where a common 
	 * invariant between the two resources needs to be kept at all times. This test uses a single transaction manager.
	 */
	public static final TwoXAResourcesWithNarayanaTransactionManagerMultiThreadedInvariantTest a050 = null;
	
}
