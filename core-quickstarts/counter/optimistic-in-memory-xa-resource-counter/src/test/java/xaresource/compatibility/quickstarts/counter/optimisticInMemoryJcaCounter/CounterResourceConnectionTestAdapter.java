/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter;

import java.util.concurrent.atomic.AtomicLong;

import javax.resource.ResourceException;
import javax.transaction.xa.XAResource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.quickstarts.counter.base.Counter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;

public class CounterResourceConnectionTestAdapter implements ResourceConnectionTestAdapter {
	
	private final static Logger logger = LoggerFactory.getLogger(CounterResourceConnectionTestAdapter.class);
	
	private final XAResourceCounterImpl.ManagedConnection connection;
	
	public CounterResourceConnectionTestAdapter(XAResourceCounterImpl.ManagedConnection connection) {
		this.connection = connection;
	}

	@Override
	public XAResource getXAResource() {
		XAResource resource = null;
		try {
			resource = connection.getXAResource();
		} catch (ResourceException e) {
			logger.error("ResourceException through when trying to retrieve the XAResource", e);
		}
		return resource;
	}

	@Override
	public int getStatus() {
		return connection.getStatus();
	}

	@Override
	public boolean verifyResourceInInitialState() {
		return getCounter().get() == 0;
	}
	
	@Override
	public void doChange1() {
		getCounter().add(1);
	}

	@Override
	public boolean verifyChange1InEffect() {
		return (getCounter().get() & 1) != 0;
	}

	@Override
	public void doChange2() {
		getCounter().add(2);
	}

	@Override
	public boolean verifyChange2InEffect() {
		return (getCounter().get() & 2) != 0;
	}

	@Override
	public void doChange3() {
		getCounter().add(4);
	}

	@Override
	public boolean verifyChange3InEffect() {
		return (getCounter().get() & 4) != 0;
	}
	
	protected Counter getCounter() {
		return connection;
	}

	public static final long invariantTestInitValue = 10000;
	// even if the ResourceConnectionTestAdapter must only ever be used by one thread at a time this instance can be handed
	//  over from one thread to another. Therefore the expectedChangeInEffectValue has to be thread safe:
	protected AtomicLong expectedChangeInEffectValue = new AtomicLong(-1L);
	
	@Override
	public void invariantTestInit(InvariantPartner invariantPartner) {
		getCounter().set(invariantTestInitValue);
	}

	@Override
	public void invariantTestMinus() {
		long l = getCounter().get();
		getCounter().add(-1000);
		expectedChangeInEffectValue.set(l - 1000);
	}

	@Override
	public void invariantTestPlus() {
		long l = getCounter().get();
		getCounter().add(1000);
		expectedChangeInEffectValue.set(l + 1000);
	}

	@Override
	public void invariantTestRead() {
		long l = getCounter().get();
		getCounter().get();
		expectedChangeInEffectValue.set(l);
	}

	@Override
	public boolean invariantTestVerifyChangeInEffect() {
		boolean result = (getCounter().get() == expectedChangeInEffectValue.get());
		if(!result)
			logger.error("invariantTestVerifyChangeInEffect(): getCounter().get(): '" + getCounter().get() + "' expectedChangeInEffectValue.get().longValue(): '" + expectedChangeInEffectValue.get() + "'");
		
		return result;
	}

	@Override
	public boolean invariantTestVerify(ResourceConnectionTestAdapter other) {
		long l1 = this.getCounter().get();
		long l2 = ((CounterResourceConnectionTestAdapter)other).getCounter().get();
		boolean result = (this.getCounter().get() + ((CounterResourceConnectionTestAdapter)other).getCounter().get()) == 2 * invariantTestInitValue;
		if(result)
			logger.debug("invariantTestVerify(): l1: '" + l1 + "' l2: '" + l2 + "' l1 + l2: '" + (l1 + l2) + "' expected: '" + (2 * invariantTestInitValue) + "'");
		else
			logger.error("invariantTestVerify(): l1: '" + l1 + "' l2: '" + l2 + "' l1 + l2: '" + (l1 + l2) + "' expected: '" + (2 * invariantTestInitValue) + "'");
		
		return result;
	}

	@Override
	public void invariantTestVerifyInTransaction(ResourceConnectionTestAdapter other, ResourceConnectionTestAdapter writeToInstance) {
		CounterResourceConnectionTestAdapter inst = (CounterResourceConnectionTestAdapter) writeToInstance;
		long l1 = this.getCounter().get();
		long l2 = ((CounterResourceConnectionTestAdapter)other).getCounter().get();
		inst.getCounter().set(l1 + l2);
		long v = inst.getCounter().get();
		logger.debug("inst.getCounter().get():" + v);
	}

	@Override
	public boolean invariantTestVerifyAfterTransactionOnWriteToInstance() {
		CounterResourceConnectionTestAdapter inst = (CounterResourceConnectionTestAdapter) this;
		boolean result = inst.getCounter().get() == 2 * invariantTestInitValue;

		if(result)
			logger.debug("invariantTestVerifyAfterTransaction(): l1 + l2: '" + inst.getCounter().get() + "' expected: '" + (2 * invariantTestInitValue) + "'");
		else
			logger.error("invariantTestVerifyAfterTransaction(): l1 + l2: '" + inst.getCounter().get() + "' expected: '" + (2 * invariantTestInitValue) + "'");
		
		return result;
	}

}
