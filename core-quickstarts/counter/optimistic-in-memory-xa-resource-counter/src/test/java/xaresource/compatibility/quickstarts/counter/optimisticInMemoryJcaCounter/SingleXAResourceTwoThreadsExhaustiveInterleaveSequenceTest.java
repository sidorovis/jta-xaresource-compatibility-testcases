/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;

import org.junit.runners.Parameterized;

import xaresource.compatibility.testcases.SupplierConstructorArguments;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;

public class SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest extends xaresource.compatibility.testcases.SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest {

	public SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest(Function<SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier) {
		super(supplier);
	}

    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		new Object[][]{
        				{((Function<SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter>)CounterResourceConnectionFactoryTestAdapter::create)}
        			}
        		);
    }
	
}
