/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter.command;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;

public class CounterCommandSet implements Command<CounterValue> {
	
	private static final long serialVersionUID = 467165593438927858L;
	
	protected final long value;
	
	public CounterCommandSet(long value) {
		this.value = value;
	}

	@Override
	public CounterValue apply(CounterValue t) {
		return t.set(value);
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
}
