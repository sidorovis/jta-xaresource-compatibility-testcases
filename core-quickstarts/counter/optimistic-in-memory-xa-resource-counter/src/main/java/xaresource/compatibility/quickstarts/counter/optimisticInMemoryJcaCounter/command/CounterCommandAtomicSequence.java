/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter.command;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;

public class CounterCommandAtomicSequence implements Command<CounterValue> {
	
	private static final long serialVersionUID = 2340374352444189105L;
	
	protected final List<Command<CounterValue>> commandSequence;
	
	public CounterCommandAtomicSequence(Collection<Command<CounterValue>> commands) {
		commandSequence = new LinkedList<Command<CounterValue>>(commands);
	}

	@Override
	public CounterValue apply(CounterValue t) {
		for(Command<CounterValue> command : commandSequence) {
			t = command.apply(t);
		}
		return t;
	}

}
