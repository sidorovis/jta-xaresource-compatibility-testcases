/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter;

import java.util.List;
import java.util.function.Supplier;

import javax.resource.ResourceException;
import javax.resource.cci.Connection;
import javax.resource.cci.ConnectionMetaData;
import javax.resource.cci.Interaction;
import javax.resource.cci.LocalTransaction;
import javax.resource.cci.ResultSetInfo;
import javax.resource.spi.ConnectionRequestInfo;
import javax.security.auth.Subject;

import xaresource.compatibility.quickstarts.counter.base.Counter;
import xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter.command.CounterValue;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.jca.AbstractOptimisticXAResourceManagedConnection;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.recoverystates.AbstractOptimisticXAResourceBase;

public class XAResourceCounterImpl extends AbstractOptimisticXAResourceBase<CounterValue, Command<CounterValue>> {

	public XAResourceCounterImpl(String id) {
		super(id, new CounterValue(0, 0));
	}

	public XAResourceCounterImpl(String id, CounterValue initValue) {
		super(id, initValue);
	}

	@Override
	protected CounterValue merge(CounterValue base, CounterValue update, List<Command<CounterValue>> commandSequence) {
		// stip off the history of the value:
		CounterValue newValue = new CounterValue(update);
		return newValue;
	}
	
	public class ManagedConnection extends AbstractOptimisticXAResourceManagedConnection<CounterValue, Command<CounterValue>> implements Counter {

		protected ManagedConnection() {
			super(XAResourceCounterImpl.this);
		}

		@Override
		public Counter reset() {
			Command<CounterValue> command = getImmutableValue().reset().getCommand();
			apply(command);
			return this;
		}

		@Override
		public Counter add(long value) {
			Command<CounterValue> command = getImmutableValue().add(value).getCommand();
			apply(command);
			return this;
		}

		@Override
		public Counter set(long value) {
			Command<CounterValue> command = getImmutableValue().set(value).getCommand();
			apply(command);
			return this;
		}

		@Override
		public long get() {
			return getImmutableValue().get();
		}

		@Override
		public Object getConnection(Subject subject, ConnectionRequestInfo cxRequestInfo) throws ResourceException {
			return new CciConnection(this);
		}

		@Override
		public void associateConnection(Object connection) throws ResourceException {
			CciConnection cciConnection = (CciConnection) connection;
			cciConnection.setManagedConnection(this);
		}
		
	}

	public static class CciConnection implements Connection, Counter {
		
		protected ManagedConnection managedConnection;
		
		protected CciConnection(ManagedConnection managedConnection) {
			this.managedConnection = managedConnection;
		}
		
		public ManagedConnection getManagedConnection() {
			return managedConnection;
		}

		public void setManagedConnection(ManagedConnection managedConnection) {
			this.managedConnection = managedConnection;
		}

		@Override
		public Counter reset() {
			return managedConnection.reset();
		}

		@Override
		public Counter add(long value) {
			return managedConnection.add(value);
		}

		@Override
		public Counter set(long value) {
			return managedConnection.set(value);
		}

		@Override
		public long get() {
			return managedConnection.get();
		}

		@Override
		public void close() throws ResourceException {
			// XXX
		}

		@Override
		public Interaction createInteraction() throws ResourceException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public LocalTransaction getLocalTransaction() throws ResourceException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ConnectionMetaData getMetaData() throws ResourceException {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public ResultSetInfo getResultSetInfo() throws ResourceException {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
	@Override
	public Supplier<javax.resource.spi.ManagedConnection> getManagedConnectionSupplier() {
		return new Supplier<javax.resource.spi.ManagedConnection>() {
			@Override
			public javax.resource.spi.ManagedConnection get() {
				return new ManagedConnection();
			}
			
		};
	}

}
