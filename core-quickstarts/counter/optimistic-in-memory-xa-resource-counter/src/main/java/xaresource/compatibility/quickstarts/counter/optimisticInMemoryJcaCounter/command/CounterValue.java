/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.quickstarts.counter.optimisticInMemoryJcaCounter.command;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.xa.Xid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import xaresource.compatibility.quickstarts.counter.base.Counter;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Command;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.Value;
import xaresource.compatibility.quickstarts.optimisticJcaXAResourceBase.ValuePlusCommandSequence;

import com.google.common.collect.Lists;
import com.thoughtworks.xstream.XStream;

import net.jcip.annotations.Immutable;

@Immutable
public final class CounterValue implements Counter, Value<CounterValue, Command<CounterValue>> {
	
	private static final long serialVersionUID = -6249944160936933114L;
	
	private final CounterValue predecessor;
	private final Command<CounterValue> predecessorPlusCommandIsThisState; 
	private final long value;
	private final long sequenceId;

	protected CounterValue(CounterValue predecessor, Command<CounterValue> command, long value, long sequenceId) {
		this.predecessor = predecessor;
		this.predecessorPlusCommandIsThisState = command;
		this.value = value;
		this.sequenceId = sequenceId;
	}

	public CounterValue(CounterValue toCopy) {
		this(null, null, toCopy.value, toCopy.sequenceId + 1);
	}
	
	public CounterValue(CounterValue predecessor, Command<CounterValue> command, long value) {
		this(predecessor, command, value, predecessor.sequenceId + 1);
	}
	
	public CounterValue(long value, long sequenceId) {
		this(null, null, value, sequenceId);
	}
	
	@Override
	public CounterValue reset() {
		Command<CounterValue> command = new CounterCommandReset();
		return new CounterValue(this, command, 0);
	}

	@Override
	public CounterValue add(long value) {
		Command<CounterValue> command = new CounterCommandAdd(value);
		return new CounterValue(this, command, this.value + value);
	}

	@Override
	public CounterValue set(long value) {
		Command<CounterValue> command = new CounterCommandSet(value);
		return new CounterValue(this, command, value);
	}

	@Override
	public long get() {
		return this.value;
	}
	
	@Override
	public Command<CounterValue> getCommand() {
		return this.predecessorPlusCommandIsThisState; 
	}
	
	public boolean isReadOnly() {
		ValuePlusCommandSequence<CounterValue, Command<CounterValue>> commandSequence = counterBaseValuePlusCommandSequence(null, this);
		return commandSequence.getCommandSequence().size() == 0;
	}
	
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
	
	public String toXML() {
		XStream xstream = new XStream();
		return xstream.toXML(this);
	}
	
	public static ValuePlusCommandSequence<CounterValue, Command<CounterValue>> counterBaseValuePlusCommandSequence(Xid xid, CounterValue counter) {
		List<Command<CounterValue>> commandSequence = new ArrayList<>();
		CounterValue end = counter;
		while(counter != null && counter.predecessorPlusCommandIsThisState != null) {
			commandSequence.add(counter.predecessorPlusCommandIsThisState);
			counter = counter.predecessor;
		}
		 
		return new ValuePlusCommandSequence<CounterValue, Command<CounterValue>>(xid, counter, end, Lists.reverse(commandSequence));
	}

	@Override
	public ValuePlusCommandSequence<CounterValue, Command<CounterValue>> getValuePlusCommandSequence(Xid xid) {
		return CounterValue.counterBaseValuePlusCommandSequence(xid, this);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		CounterValue rhs = (CounterValue) obj;
		return new EqualsBuilder()
			.appendSuper(super.equals(obj))
			.append(value, rhs.value)
			.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(value).toHashCode();
	}

}
