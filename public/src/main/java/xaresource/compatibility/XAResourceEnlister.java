/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility;

import javax.resource.ResourceException;
import javax.resource.spi.ConnectionManager;
import javax.resource.spi.ConnectionRequestInfo;
import javax.resource.spi.ManagedConnection;
import javax.resource.spi.ManagedConnectionFactory;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XAResourceEnlister  implements ConnectionManager {

	private static final Logger logger = LoggerFactory.getLogger(XAResourceEnlister.class);
	
	private static final long serialVersionUID = 1L;
	
	private final TransactionManager transactionManager;
	
	public XAResourceEnlister(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	@Override
	public Object allocateConnection(ManagedConnectionFactory mcf, ConnectionRequestInfo cxRequestInfo) throws ResourceException {
		ManagedConnection managedConnection = mcf.createManagedConnection(null, null);
		XAResource xaResource = managedConnection.getXAResource();
		try {
			int status = transactionManager.getStatus();
			if(status == Status.STATUS_NO_TRANSACTION) {
				// no transaction is in place, therefore not doing anything
			} else {
				transactionManager.getTransaction().enlistResource(xaResource);					
			}
		} catch (IllegalStateException | RollbackException | SystemException e) {
			String msg = "While trying to enlist the resource in the transaction an exception was raised!";
			logger.error("", e);
			throw new ResourceException(msg, e);
		}
		return managedConnection.getConnection(null, null);
	}
	
}
