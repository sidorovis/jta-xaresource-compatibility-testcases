/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases.helpers;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

public class InterleaveSequenceGeneratorTest {

	/*
	 * https://code.google.com/p/combinatoricslib/
	 */
	//@Test
	public void test() {
		String[] numbers = {"1", "2", "3", "4", "5"};
		String[] letters = {"a", "b", "c", "d"};
		
		ICombinatoricsVector<Integer> initialVector = Factory.createVector(new Integer[] {0, 1, 2, 3, 4, 5, 6, 7, 8} );
		Generator<Integer> gen = Factory.createSimpleCombinationGenerator(initialVector, 5);
		
		for (ICombinatoricsVector<Integer> combination : gen) {
			String[] result = new String[9];
			int j = 0;
			int k = 0;
			for(int i = 0; i < 9; i++) {
				if(j < 5 && i == combination.getValue(j).intValue()) {
					// take from 'left' array
					result[i] = numbers[j];
					j++;
				} else {
					result[i] = letters[k];
					k++;
				}
			}
			String join = String.join("", result);
			System.out.println(join);
		}
	}
	
	protected long factorial(long n) {
		long r = 1;
		for(long i = 1; i <= n; i++) r = r * i;
		return r;
	}
	
	protected long binom(long n, long k) {
		long r = 1;
		for(long i = k; i <= n; i++) r = r * i;
		long denominator = factorial(k);
		return r/denominator;
	}

	@Test
	public void test2() {
		String[] numbers = {"1", "2", "3", "4", "5"};
		String[] letters = {"a", "b", "c", "d"};
		
		String[][] results = InterleaveSequenceGenerator.generate(numbers, letters, String[]::new, String[][]::new);
		for(String[] result: results) {
			System.out.println(String.join("", result));
		}
		
		assertThat("The number of elements should match \binom{9}{5})", (long)results.length, is(binom(numbers.length + letters.length, numbers.length)));
	}
	

}
