/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.assertTrue;

import javax.transaction.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;

public class TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTestDoWork {
	
	private final Logger logger = LoggerFactory.getLogger(TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTestDoWork.class);
	
	public TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTestDoWork() {
		logger.debug("Created instance of 'XADistributedTransactionSpringDeclarativeTransactionTestSuiteDoWork'.");
	}

    //@Inject // works, too
    //private JtaTransactionManager platformTransactionManager;

	@Transactional
	public void doWork(ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter1, ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter2) {
		// http://java.dzone.com/articles/monitoring-declarative-transac
		assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
		
		/*
		try {
			platformTransactionManager.getTransactionManager().getTransaction().enlistResource(xaResourceAdapter1.getXAResource());
        	platformTransactionManager.getTransactionManager().getTransaction().enlistResource(xaResourceAdapter2.getXAResource());
		} catch (Exception e) {
			fail("No exception is expected here.");			
		}
		 */
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		
		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
		resourceConnectionTestAdapter1.doChange1();
		assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
		resourceConnectionTestAdapter2.doChange1();
		assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
	}

	@Transactional
	public void failWork(ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter1, ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter2) {
		// http://java.dzone.com/articles/monitoring-declarative-transac
		assertTrue(TransactionSynchronizationManager.isActualTransactionActive());
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		
		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
		resourceConnectionTestAdapter1.doChange1();
		assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
		resourceConnectionTestAdapter2.doChange1();
		assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
		
		throw new RuntimeException("This exception will roll back the transaction.");
	}
	
}
