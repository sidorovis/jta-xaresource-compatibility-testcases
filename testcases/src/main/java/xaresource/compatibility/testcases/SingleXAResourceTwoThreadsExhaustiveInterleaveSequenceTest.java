/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter.TransactionStartsWith;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.categories.SpeedSlow;
import xaresource.compatibility.testcases.helpers.InterleaveSequenceGenerator;

@RunWith(Parameterized.class)
@Category(SpeedSlow.class)
public class SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest {

	private static final Logger logger = LoggerFactory.getLogger(SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest.class);
	
	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter;

	public SingleXAResourceTwoThreadsExhaustiveInterleaveSequenceTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier) {
		this.supplier = supplier;
	}
	
	@Before
	public void setUp() {
		resourceConnectionFactoryTestAdapter = supplier.apply(new SupplierConstructorArguments());
	}
	
	@Test
	public void testCommitInterleaveSequences() {
		
		Xid xid1 = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		XAResourceCommitSequenceSteps leftCommitSequenceSteps = new XAResourceCommitSequenceSteps("leftCommitSequenceSteps", xid1, 1);
		
		Xid xid2 = new MyXid(100, new byte[]{0x02}, new byte[]{0x03});
		XAResourceCommitSequenceSteps rightCommitSequenceSteps = new XAResourceCommitSequenceSteps("rightCommitSequenceSteps", xid2, 2);
		
		final Step[] leftSteps  = leftCommitSequenceSteps.getSteps();
		final Step[] rightSteps = rightCommitSequenceSteps.getSteps();
		
		Step[][] exhaustiveInterleaveSequence = 
				InterleaveSequenceGenerator.generate(leftSteps, rightSteps, Step[]::new, Step[][]::new);
		
		logger.info("Going to iterate over : '" + exhaustiveInterleaveSequence.length + "' interleaveSequences");
		
		int interleaveSequenceIndex = 0;
		for(Step[] interleaveSequence : exhaustiveInterleaveSequence) {
			executeInterleaveSequence(
				new InterleaveSequenceDefinition(
						supplier, 
						leftCommitSequenceSteps, 
						rightCommitSequenceSteps, 
						interleaveSequence, 
						interleaveSequenceIndex++,
						() -> new XAResourceSequenceStepsCrossThreadCommunicationContext() // this supplier generates new contexts
						)
			);
		}
	}
	
	public static void executeInterleaveSequence(InterleaveSequenceDefinition sequenceDefinition) {
		logger.info("Starting interleaveSequence: " + sequenceDefinition.index);
		
		// a sequence is always executed with a fresh XAResourceTestAdapter, which means also a fresh
		// XAResource instance.
		// The left and right sequences are initialized with this test adapter so that they work on it.
		Object crossThreadCommunicationContext = sequenceDefinition.crossThreadCommunicationContext.get();
		ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter = sequenceDefinition.supplier.apply(new SupplierConstructorArguments());
		sequenceDefinition.left.init(resourceConnectionFactoryTestAdapter, crossThreadCommunicationContext);
		sequenceDefinition.right.init(resourceConnectionFactoryTestAdapter, crossThreadCommunicationContext);
		
		int numberOfPromises = sequenceDefinition.sequence.length + 1;
		@SuppressWarnings("unchecked")
		CompletableFuture<Integer>[] promises = new CompletableFuture[numberOfPromises];
		for(int i=0; i < numberOfPromises; i++) {
			promises[i] = new CompletableFuture<Integer>(); 
		}
		CompletableFuture<Integer> startPromise = promises[0]; 
		CompletableFuture<Integer> endPromise = promises[promises.length - 1];
		for(int i = 0; i < sequenceDefinition.sequence.length; i++) {
			Step step = sequenceDefinition.sequence[i];
			step.setInputStep(promises[i]);
			step.setOutputStep(promises[i + 1]);
		}
		
		final Step[] leftSteps  = sequenceDefinition.left.getSteps();
		final Step[] rightSteps = sequenceDefinition.right.getSteps();
		
		Thread thread1 = new Thread(
			() -> {
				for(int i = 0; i < leftSteps.length; i++) {
					leftSteps[i].run();
				}
			}
		);

		Thread thread2 = new Thread(
				() -> {
					for(int i = 0; i < rightSteps.length; i++) {
						rightSteps[i].run();
					}
				}
			);
		thread1.start();
		thread2.start();
		
		// start the interleave sequence in the two threads
		startPromise.complete(1);
		
		try {
			try {
				assertThat("The number of the last promise should be the length of the promise array.", endPromise.get(), is(promises.length));
			} catch (InterruptedException | ExecutionException e) {
				String msg = "An exception was thrown while verifying the last promise in the sequence!";
				logger.error(msg, e);
				fail(msg);
			}
			
			if(null != sequenceDefinition.left.getThrowable()) {
				String msg = "There should not have been any exceptions while executing sequenceDefinition.left";
				logger.error(msg, sequenceDefinition.left.getThrowable());
				assertNull(msg, sequenceDefinition.left.getThrowable());
			}

			if(null != sequenceDefinition.right.getThrowable()) {
				String msg = "There should not have been any exceptions while executing sequenceDefinition.right";
				logger.error(msg, sequenceDefinition.right.getThrowable());
				assertNull(msg, sequenceDefinition.right.getThrowable());
			}
		} catch(Throwable t) {
			logger.error("sequenceDefinition sequenceSteps:"  + sequenceDefinition, t);
			throw t;
		}
	}
	
	public static class Step implements Runnable, Serializable {
		
		private static final long serialVersionUID = 1L;
		
		protected final AtomicReference<CompletableFuture<Integer>> inputStep = new AtomicReference<CompletableFuture<Integer>>();
		protected final AtomicReference<CompletableFuture<Integer>> outputStep = new AtomicReference<CompletableFuture<Integer>>();
		protected final Runnable action;
		protected final String stepName;
		
		public Step(String stepName, Runnable action) {
			this.action = action;
			this.stepName = stepName;
		}
		
		public void setInputStep(CompletableFuture<Integer> inputStep) {
			this.inputStep.set(inputStep);
		}

		public void setOutputStep(CompletableFuture<Integer> outputStep) {
			this.outputStep.set(outputStep);
		}
		
		@Override
		public void run() {
			int input = -1;
			try {
				input = inputStep.get().get();
			} catch (InterruptedException | ExecutionException e) {
				logger.error("While reading from the inputStep an exception was thrown:", e);
			}
			logger.debug("Starting step number: " + input);
			logger.debug("Starting step: " + this);
			try {
				action.run();
			} catch (Throwable t) {
				logger.debug("Caught exception while executing step: " + this, t);
			}
			logger.debug("Finishing step: " + input);
			outputStep.get().complete(input + 1);
		}
		
		@Override
		public String toString() {
			return "Step[" + stepName + "]";
		}
		
	}

	public static abstract class SequenceSteps implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		// the ex variable will be read outside of the step executing thread in the "main" thread to verify if the
		// execution was successful.
		protected AtomicReference<Throwable> ex = new AtomicReference<Throwable>();
		
		// the xaResourceAdapter variable will be written from outside of the step executing thread in order to initialize
		// the step sequence
		protected AtomicReference<ResourceConnectionFactoryTestAdapter> connectionFactory = new AtomicReference<ResourceConnectionFactoryTestAdapter>();
		protected ResourceConnectionTestAdapter xaResourceAdapter = null;
		protected Object crossThreadCommunicationContext = null;
		
		public void catchAndHandleExceptions(Runnable action) {
			if(ex.get() != null) return;
			try {
				action.run();
			} catch (Throwable t) {
				if(ex.get() != null) t.addSuppressed(ex.get());
				ex.set(t);
				throw t;
			}
		}
		
		public Throwable getThrowable() {
			return ex.get();
		}
		
		public void init(ResourceConnectionFactoryTestAdapter connectionFactory, Object crossThreadCommunicationContext) {
			ex.set(null);
			this.connectionFactory.set(connectionFactory);
			this.crossThreadCommunicationContext = crossThreadCommunicationContext;
			assertTrue(this.connectionFactory.get().getResourceConnectionTestAdapter().verifyResourceInInitialState());
		}
		
		public abstract Step[] getSteps();
	}
	
	public static class XAResourceSequenceStepsCrossThreadCommunicationContext {
		protected List<SequenceSteps> participatingSequenceSteps = new ArrayList<>();
		protected SequenceSteps winner = null;
		
		public synchronized void registerParticipant(SequenceSteps participant) {
			participatingSequenceSteps.add(participant);
		}
		
		public synchronized boolean amISucceedingAsWinnerForTheRunToPrepare(SequenceSteps participant) {
			if(null == winner) {
				winner = participant;
				return true;
			} else {
				return false;				
			}
		}

		public synchronized boolean amISucceedingAsWinnerForTheRunToPrepareBecauseICalledStartFirst(SequenceSteps participant) {
			return participatingSequenceSteps.get(0) == participant;
		}
		
		public synchronized void removeMeFromContextIfIAmStillTheOnlyParticipant(SequenceSteps participant) {
			if(participatingSequenceSteps.size() == 1) {
				if(participatingSequenceSteps.contains(participant)) {
					// clear the list so that the other sequence can work on the context as if it were the first:
					participatingSequenceSteps.clear();
					winner = null;
				} else {
					fail("This is a bug in the protocol; you should only ask if you are the winner after you've registered as a participant.");
				}
			}
		}
		
	}
	
	public static class XAResourceCommitSequenceSteps extends SequenceSteps {
		
		private static final long serialVersionUID = 1L;
		
		protected final Xid xid;
		protected final String sequenceName;
		protected final int change;
		int ret = -1;

		boolean rollback = false;
				
		protected final Step[] steps;
		
		protected void addStep(ArrayList<Step> stepArray, String stepName, Runnable step) {
			stepArray.add(new Step(sequenceName + "." + stepName, () -> catchAndHandleExceptions(step)));			
		}
		
		@Override
		public void init(ResourceConnectionFactoryTestAdapter connectionFactory, Object crossThreadCommunicationContext) {
			super.init(connectionFactory, crossThreadCommunicationContext);
			rollback = false;
		}
		
		public XAResourceCommitSequenceSteps(String sequeneName, Xid xid, int change) {
			this.sequenceName = sequeneName;
			this.xid = xid;
			this.change = change;
			ArrayList<Step> stepArray = new ArrayList<>();
			addStep(stepArray, "step1Start()", () -> step1Start());
			addStep(stepArray, "step2DoChange1()", () -> step2DoChange1());
			addStep(stepArray, "step3VerifyChange1InEffect()", () -> step3VerifyChange1InEffect());
			addStep(stepArray, "step4End()", () -> step4End());
			addStep(stepArray, "step5Prepare()", () -> step5Prepare());
			addStep(stepArray, "step6Commit()", () -> step6Commit());
			//addStep(stepArray, "step7Close()", () -> step7Close());
			addStep(stepArray, "step8VerifyEndState()", () -> step8VerifyEndState());
			
			this.steps = new Step[stepArray.size()];
			stepArray.toArray(steps);
		}
		
		protected XAResourceSequenceStepsCrossThreadCommunicationContext getContext() {
			return (XAResourceSequenceStepsCrossThreadCommunicationContext) crossThreadCommunicationContext;
		}

		@Override
		public Step[] getSteps() {
			return steps;
		}

		public void step1Start() {
			try {
				xaResourceAdapter = connectionFactory.get().getResourceConnectionTestAdapter();
				xaResourceAdapter.getXAResource().start(xid, XAResource.TMNOFLAGS);
				
				if(connectionFactory.get().getTransactionStartsWith() == TransactionStartsWith.START)
					this.getContext().registerParticipant(this);
			} catch (XAException e) {
				ex.set(e);
			}			
		}
		
		public void step2DoChange1() {
			// it may happen that during the change the xaresource is already recognizing that
			// that this attempt for changing will lead to a rollback case. In the sense of fail-fast
			// this is a good decision to already throw an exception here.
			// XXX verify in such cases that all resources associated to this transaction xid or attached 
			//     to ThreadLocal variables are cleaned-up!
			
			if(connectionFactory.get().getTransactionStartsWith() == TransactionStartsWith.CHANGE)
				this.getContext().registerParticipant(this);
			try {
				switch(change) {
				case 1:
					xaResourceAdapter.doChange1();
					break;
				case 2:
					xaResourceAdapter.doChange2();
					break;
				case 3:
					xaResourceAdapter.doChange3();
					break;
				}				
			} catch(Throwable t) {
				if(this.getContext().amISucceedingAsWinnerForTheRunToPrepare(this)) {
					if(this.getContext().amISucceedingAsWinnerForTheRunToPrepareBecauseICalledStartFirst(this)) {
						fail("The other thread did not succeed yet with the prepare but I get a exception here.");
						ex.set(t);
					} else {
						logger.debug("It is unclear if a transaction should end in a rollback only because the other transaction started before me. Perhaps this other transaction needs much longer and I might be quicker at the prepare() step?");
					}
				}
				rollback = true;
			}
		}
		
		public void step3VerifyChange1InEffect() {
			if(false == rollback) {
				switch(change) {
				case 1:
					assertTrue(xaResourceAdapter.verifyChange1InEffect());			
					break;
				case 2:
					assertTrue(xaResourceAdapter.verifyChange2InEffect());			
					break;
				case 3:
					assertTrue(xaResourceAdapter.verifyChange3InEffect());			
					break;
				}
			}
		}
		
		public void step4End() {
			if(false == rollback) {
				try {
					xaResourceAdapter.getXAResource().end(xid, XAResource.TMSUCCESS);
				} catch (XAException e) {
					ex.set(e);
				}			
			}
		}
		
		public void step5Prepare() {
			if(false == rollback) {
				boolean amISucceedingAsWinner = this.getContext().amISucceedingAsWinnerForTheRunToPrepare(this);
				try {
					ret = xaResourceAdapter.getXAResource().prepare(xid);
					if(!amISucceedingAsWinner)
						fail("I am not supposed to succeed in the prepare call!");
				} catch (XAException e) {
					logger.debug("Rollback: Caught an XAException during the call to prepare()");
					if(amISucceedingAsWinner) {
						ex.set(e); // I am supposed to succeed, because I am the first thread in the prepare call!					
					}
					rollback = true;
				} catch (Throwable t) {
					if(amISucceedingAsWinner) {
						ex.set(t); // I am supposed to succeed, because I am the first thread in the prepare call!					
					}				
				}
			}
		}
		
		public void step6Commit() {
			this.getContext().removeMeFromContextIfIAmStillTheOnlyParticipant(this);			
			if(false == rollback) {
				if (ret == XAResource.XA_OK) {
					try {
						xaResourceAdapter.getXAResource().commit(xid, false);
					} catch (XAException e) {
						ex.set(e);
					}
				} else {
					fail("Expected to get a 'ret == XAResource.XA_OK'.");				
				}
			}
		}
		
		public void step7Close() {
			/*
			try {
				xaResourceAdapter.get().close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
			*/			
		}
		
		public void step8VerifyEndState() {
			if(false == rollback) {
				switch(change) {
				case 1:
					assertTrue(xaResourceAdapter.verifyChange1InEffect());			
					break;
				case 2:
					assertTrue(xaResourceAdapter.verifyChange2InEffect());			
					break;
				case 3:
					assertTrue(xaResourceAdapter.verifyChange3InEffect());			
					break;
				}				
			}
		}
		
		
	}
	
	public static class InterleaveSequenceDefinition implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		public final long index;
		public final Step[] sequence;
		public final SequenceSteps left;
		public final SequenceSteps right;
		public final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier;
		public final Supplier<? extends Object> crossThreadCommunicationContext;
		
		public InterleaveSequenceDefinition(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier, SequenceSteps left, SequenceSteps right, Step[] sequence, long index, Supplier<? extends Object> crossThreadCommunicationContext) {
			this.supplier = supplier;
			this.sequence = sequence;
			this.left = left;
			this.right = right;
			this.index = index;
			this.crossThreadCommunicationContext = crossThreadCommunicationContext;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			for(Step step : sequence) {
				sb.append("\n" + step);
			}
			return sb.toString();
		}
		
	}
	
	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		//new Object[]{((Supplier<XAResourceTestAdapter>)TransactionalMapXAResourceTestAdapter::create)},
        		//new Object[]{((Supplier<XAResourceTestAdapter>)XAResourceCounterTestAdapter::create)}
        		new Object[][]{
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        				//{(Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create}
        				//{(Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create}
        			}
        		);
    }
    */
	
}
