/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.resource.spi.ConnectionManager;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import javax.transaction.xa.XAResource;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import xaresource.compatibility.XAResourceEnlister;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.categories.SpeedFast;

import com.arjuna.ats.arjuna.common.CoreEnvironmentBean;
import com.arjuna.ats.arjuna.common.CoreEnvironmentBeanException;
import com.arjuna.ats.arjuna.common.arjPropertyManager;
import com.arjuna.ats.arjuna.recovery.RecoveryDriver;
import com.arjuna.ats.arjuna.recovery.RecoveryManager;
import com.arjuna.ats.arjuna.recovery.RecoveryModule;
import com.arjuna.ats.arjuna.tools.RecoveryMonitor;
import com.arjuna.ats.internal.jta.recovery.arjunacore.XARecoveryModule;
import com.arjuna.ats.jta.common.JTAEnvironmentBean;
import com.arjuna.ats.jta.common.jtaPropertyManager;
import com.arjuna.common.internal.util.propertyservice.BeanPopulator;

@SuppressWarnings("unused")
@RunWith(Parameterized.class)
@Category(SpeedFast.class)
public class TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedTest {

	private static final Logger logger = LoggerFactory.getLogger(TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedTest.class);

	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter1;

	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter2;
	
	protected TransactionManager transactionManager;
	
	public static final String nodeName = "myTestNode";
	
	public TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1, Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2) {
		this.supplier1 = supplier1;
		this.supplier2 = supplier2;
	}
	
	@Before
	public void setUp() throws CoreEnvironmentBeanException {
		XAResourceEnlister enlister = new XAResourceEnlister(com.arjuna.ats.jta.TransactionManager.transactionManager());
		
		resourceConnectionFactoryTestAdapter1 = supplier1.apply(new SupplierConstructorArguments(enlister));
		resourceConnectionFactoryTestAdapter2 = supplier2.apply(new SupplierConstructorArguments(enlister));

		String nodeName = "myTestNode";
		
		
		CoreEnvironmentBean coreEnvironmentBean = arjPropertyManager.getCoreEnvironmentBean();
		coreEnvironmentBean.setNodeIdentifier(nodeName);
		
		/*
		JTAEnvironmentBean jtaEnvironmentBean = jtaPropertyManager.getJTAEnvironmentBean();
		jtaEnvironmentBean.setXaRecoveryNodes(Arrays.asList(nodeName));
		RecoveryManager.manager().scan();
		*/
				
		transactionManager = com.arjuna.ats.jta.TransactionManager.transactionManager();
	}
	
	// From project narayana, under directory /ArjunaJTS/jtax/tests/classes/com/arjuna/ats/jtax/tests/implicit/impl/.
	// https://github.com/ochaloup/quickstart-jbosstm/blob/master/ArjunaJTS/trailmap/src/main/java/com/arjuna/demo/recovery/xaresource/TestXAResourceRecovery.java
	@Test
	public void whenModifyingTwoIndependentXAResourcesAndCommittingBothNewValuesWillBeVisible() {
		logger.debug("whenModifyingTwoIndependentXAResourcesAndCommittingBothNewValuesWillBeVisible");
		try {
			UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
			ut.begin();
			//Transaction tx = transactionManager.getTransaction();
			
			// XXX these calls have to be here; this corresponds to the getConnection() call that also takes care of resource enlistment
			//     this has to happen after the transaction is started.
			ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
			ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			
			// System.out.println("CurrentTx : "+tx+" >> "+OTSImpleManager.current().getControlWrapper().isLocal());
			//tx.enlistResource(xaResourceAdapter1.getXAResource());
	        //tx.enlistResource(xaResourceAdapter2.getXAResource());
	        
    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
    		resourceConnectionTestAdapter1.doChange1();
			assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
    		resourceConnectionTestAdapter2.doChange1();
			assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
	        
	        ut.commit();
	        /*
	        tx.delistResource(xaResourceAdapter1.getXAResource(),javax.transaction.xa.XAResource.TMSUCCESS);
	        tx.delistResource(xaResourceAdapter2.getXAResource(),javax.transaction.xa.XAResource.TMSUCCESS);
	        */			
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");			
		}
		// XXX We have to get a new connection outside of the transaction scope to verify outside of the transaction scope that the results are in effect
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
		assertTrue(resourceConnectionTestAdapter2.getStatus() == Status.STATUS_NO_TRANSACTION);
	}

	@Test
	public void whenModifyingTwoIndependentXAResourcesAndCommittingViaSpringTransactionTemplateBothNewValuesWillBeVisible() {
		logger.debug("whenModifyingTwoIndependentXAResourcesAndCommittingViaSpringTransactionTemplateBothNewValuesWillBeVisible");
		// references so that you can quickly jump to these classes in order to set breakpoints:
		com.arjuna.ats.internal.jta.transaction.arjunacore.UserTransactionImple ut_ = null;
		com.arjuna.ats.internal.jta.transaction.arjunacore.TransactionManagerImple tm_ = null;
		com.arjuna.ats.internal.jta.transaction.arjunacore.BaseTransaction bt_ = null;
		try {
			UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
			JtaTransactionManager platformTransactionManager = new JtaTransactionManager(ut, transactionManager);
			
			TransactionTemplate transactionTemplate = new TransactionTemplate(platformTransactionManager);
			
			// http://stackoverflow.com/questions/4953509/approach-to-automatically-enlist-cleanup-xaresources-into-new-tx
			// http://stackoverflow.com/questions/710859/what-are-the-benefits-of-jca
			// http://www.javabeat.net/j2ee-connector-architecturejca-an-introduction/
			transactionTemplate.execute(new TransactionCallback<Object>() {
	            // the code in this method executes in a transactional context
	            public Object doInTransaction(TransactionStatus status) {
	            	/*
	            	try {
						platformTransactionManager.getTransactionManager().getTransaction().enlistResource(xaResourceAdapter1.getXAResource());
		            	platformTransactionManager.getTransactionManager().getTransaction().enlistResource(xaResourceAdapter2.getXAResource());
					} catch (Exception e) {
						fail("No exception is expected here.");			
						status.setRollbackOnly();
						return null;
					}
					*/
	            	
	    			ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
	    			ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
	            	
	            	
	        		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
	        		resourceConnectionTestAdapter1.doChange1();
	    			assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
	        		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
	        		resourceConnectionTestAdapter2.doChange1();
	    			assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
	                return null;
	            }
	        });
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");			
		}
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
		assertTrue(resourceConnectionTestAdapter2.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	@Test
	public void whenModifyingTwoIndependentXAResourcesAndRollingBackThenNoneOfTheChangesWillBeVisible() {
		logger.debug("whenModifyingTwoIndependentXAResourcesAndRollingBackThenNoneOfTheChangesWillBeVisible");
		try {
			UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
			ut.begin();
			
			ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
			ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			
			// System.out.println("CurrentTx : "+tx+" >> "+OTSImpleManager.current().getControlWrapper().isLocal());
			//tx.enlistResource(xaResourceAdapter1.getXAResource());
	        //tx.enlistResource(xaResourceAdapter2.getXAResource());
	        
    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
    		resourceConnectionTestAdapter1.doChange1();
			assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
    		resourceConnectionTestAdapter2.doChange1();
			assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
	        
	        ut.rollback();
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");			
		}
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter2.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
		assertTrue(resourceConnectionTestAdapter2.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	

	@Test
	public void whenModifyingTwoIndependentXAResourcesAndRollingBackViaSpringTransactionTemplateThenNoneOfTheChangesWillBeVisible() {
		logger.debug("whenModifyingTwoIndependentXAResourcesAndRollingBackViaSpringTransactionTemplateThenNoneOfTheChangesWillBeVisible");
		try {
			UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
			JtaTransactionManager platformTransactionManager = new JtaTransactionManager(ut, transactionManager);
			
			TransactionTemplate transactionTemplate = new TransactionTemplate(platformTransactionManager);
			
			transactionTemplate.execute(new TransactionCallback<Object>() {
	            // the code in this method executes in a transactional context
	            public Object doInTransaction(TransactionStatus status) {
	            	/*
	            	try {
						platformTransactionManager.getTransactionManager().getTransaction().enlistResource(xaResourceAdapter1.getXAResource());
		            	platformTransactionManager.getTransactionManager().getTransaction().enlistResource(xaResourceAdapter2.getXAResource());
					} catch (Exception e) {
						fail("No exception is expected here.");			
						status.setRollbackOnly();
						return null;
					}
					*/

	    			ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
	    			ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();

	        		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
	        		resourceConnectionTestAdapter1.doChange1();
	    			assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
	        		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
	        		resourceConnectionTestAdapter2.doChange1();
	    			assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
	    			
	    			status.setRollbackOnly();
	                return null;
	            }
	        });
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");			
		}
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter2.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
		assertTrue(resourceConnectionTestAdapter2.getStatus() == Status.STATUS_NO_TRANSACTION);
	}

	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		//new Object[]{((Supplier<XAResourceTestAdapter>)TransactionalMapXAResourceTestAdapter::create)},
        		//new Object[]{((Supplier<XAResourceTestAdapter>)XAResourceCounterTestAdapter::create)}
        		new Object[][]{
        				//{((Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create), ((Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create)},
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create), ((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        			}
        		);
    }
    */
}
