/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.function.Function;

import javax.transaction.Status;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.categories.SpeedFast;

@RunWith(Parameterized.class)
@Category(SpeedFast.class)
public class SingleXAResourceSinglethreadedAdvancedTest {

	private static final Logger logger = LoggerFactory.getLogger(SingleXAResourceSinglethreadedAdvancedTest.class);
	
	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter;

	public SingleXAResourceSinglethreadedAdvancedTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier) {
		this.supplier = supplier;
	}
	
	@Before
	public void setUp() {
		resourceConnectionFactoryTestAdapter = supplier.apply(new SupplierConstructorArguments());
	}

	@Test
	public void whenOperatingOnSingleXAResourceWithTwoIndependentTransactionsWhereFirstCommitAndSecondAbortThenOnlyEffectOfFirstTransactionWillBeVisible() {
		Xid xid1 = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		Xid xid2 = new MyXid(100, new byte[]{0x01}, new byte[]{0x03});
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		
		int ret = -1;
		
		try {
			xaresource.start(xid1, XAResource.TMNOFLAGS);			
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource.end(xid1, XAResource.TMSUCCESS);
			
			xaresource.start(xid2, XAResource.TMNOFLAGS);
			
			ret = xaresource.prepare(xid1);
			if (ret == XAResource.XA_OK) {
				xaresource.commit(xid1, false);
			}

			resourceConnectionTestAdapter.doChange2();
			assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
			xaresource.end(xid2, XAResource.TMSUCCESS);
			
			if(resourceConnectionFactoryTestAdapter.isChange2Independent()) {
				ret = xaresource.prepare(xid2);			
				xaresource.rollback(xid2);				
			} else {
				try {
					xaresource.prepare(xid2);
					fail("Expect to cause an XAException but did not trigger an exception.");
				} catch(XAException e) {
					// good case
				} catch(Exception e) {
					fail("Expect to cause an XAException but got other exception.");					
				}
			}
		}  catch (Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
		if(resourceConnectionFactoryTestAdapter.isChange2Independent())
			assertFalse(resourceConnectionTestAdapter.verifyChange2InEffect());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	@Test
	public void whenOperatingOnSingleXAResourceAsIfTwoAndJoiningTransactionThenNewValuesWillBeVisible() {
		Xid xid1 = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		Xid xid2 = new MyXid(100, new byte[]{0x01}, new byte[]{0x03});
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource1 = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		XAResource xaresource2 = xaresource1;
		
		int ret = -1;
		
		try {
			xaresource1.start(xid1, XAResource.TMNOFLAGS);			
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource1.end(xid1, XAResource.TMSUCCESS);
			
			if (xaresource2.isSameRM(xaresource1)) {
				xaresource2.start(xid1, XAResource.TMJOIN);
				resourceConnectionTestAdapter.doChange2();
				assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
				xaresource2.end(xid1, XAResource.TMSUCCESS);
			} else {
				xaresource2.start(xid2, XAResource.TMNOFLAGS);
				resourceConnectionTestAdapter.doChange2();
				assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
				xaresource2.end(xid2, XAResource.TMSUCCESS);
				ret = xaresource2.prepare(xid2);
				if (ret == XAResource.XA_OK) {
					xaresource2.commit(xid2, false);
				} else {
					fail("Expected to get a 'ret == XAResource.XA_OK'.");				
				}
			}
			
			ret = xaresource1.prepare(xid1);
			if (ret == XAResource.XA_OK) {
				xaresource1.commit(xid1, false);
			} else {
				fail("Expected to get a 'ret == XAResource.XA_OK'.");				
			}
		}  catch (Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
			
	@Test
	public void whenOperatingOnSingleXAResourceAndSuspendingAndResumingAndDoingIndependentWorkInBetweenAndRollingBackTransactionThenOnlyIndependentWorkWillBeVisible() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());

		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource.end(xid, XAResource.TMSUSPEND);

			if(!resourceConnectionFactoryTestAdapter.isChange2Independent() && resourceConnectionFactoryTestAdapter.isPessimisticLocking()) {
				// do nothing, because otherwise this will lock
			} else {
				resourceConnectionTestAdapter.doChange2();
				assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());				
			}

			xaresource.start(xid, XAResource.TMRESUME);
			resourceConnectionTestAdapter.doChange3();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			if(resourceConnectionFactoryTestAdapter.isChange2Independent())
				assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
			assertTrue(resourceConnectionTestAdapter.verifyChange3InEffect());
			xaresource.end(xid, XAResource.TMSUCCESS);
			
			if(resourceConnectionFactoryTestAdapter.isChange2Independent() || resourceConnectionFactoryTestAdapter.isPessimisticLocking()) {
				xaresource.prepare(xid);
				xaresource.rollback(xid);				
			} else {
				try {
					xaresource.prepare(xid);
					fail("Expect to cause an XAException but did not trigger an exception.");
				} catch(XAException e) {
					// good case
				} catch(Exception e) {
					fail("Expect to cause an XAException but got other exception.");					
				}
			}			
		} catch (XAException e) {
			logger.error("XAException was thrown!", e);
			fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertFalse(resourceConnectionTestAdapter.verifyChange1InEffect());
		assertTrue(resourceConnectionFactoryTestAdapter.isPessimisticLocking() || resourceConnectionTestAdapter.verifyChange2InEffect());
		assertFalse(resourceConnectionTestAdapter.verifyChange3InEffect());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	@Test
	public void whenOperatingOnSingleXAResourceAndSuspendingAndResumingAndDoingIndependentWorkInBetweenAndCommittingTransactionThenAllWorkWillBeVisible() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());

		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource.end(xid, XAResource.TMSUSPEND);

			if(!resourceConnectionFactoryTestAdapter.isChange2Independent() && resourceConnectionFactoryTestAdapter.isPessimisticLocking()) {
				// do nothing, because otherwise this will lock
			} else {
				resourceConnectionTestAdapter.doChange2();
				assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());				
			}

			xaresource.start(xid, XAResource.TMRESUME);
			resourceConnectionTestAdapter.doChange3();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			if(resourceConnectionFactoryTestAdapter.isChange2Independent())
				assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
			assertTrue(resourceConnectionTestAdapter.verifyChange3InEffect());
			xaresource.end(xid, XAResource.TMSUCCESS);
			
			
			if(resourceConnectionFactoryTestAdapter.isChange2Independent() || resourceConnectionFactoryTestAdapter.isPessimisticLocking()) {
				int ret = xaresource.prepare(xid);
				if (ret == XAResource.XA_OK) {
					xaresource.commit(xid, false);
				} else {
					fail("Expected to get a 'ret == XAResource.XA_OK'.");				
				}
			} else {
				try {
					xaresource.prepare(xid);
					fail("Expect to cause an XAException but did not trigger an exception.");
				} catch(XAException e) {
					// good case
				} catch(Exception e) {
					fail("Expect to cause an XAException but got other exception.");					
				}
			}			
		} catch (Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		} /*finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		if(resourceConnectionFactoryTestAdapter.isChange2Independent()) {			
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			assertTrue(resourceConnectionTestAdapter.verifyChange2InEffect());
			assertTrue(resourceConnectionTestAdapter.verifyChange3InEffect());
		} else {
			assertFalse(resourceConnectionTestAdapter.verifyChange1InEffect());
			assertTrue(resourceConnectionFactoryTestAdapter.isPessimisticLocking() || resourceConnectionTestAdapter.verifyChange2InEffect());
			assertFalse(resourceConnectionTestAdapter.verifyChange3InEffect());			
		}
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}

	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		//new Object[]{((Supplier<XAResourceTestAdapter>)TransactionalMapXAResourceTestAdapter::create)},
        		//new Object[]{((Supplier<XAResourceTestAdapter>)XAResourceCounterTestAdapter::create)}
        		new Object[][]{
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        				}
    		);
    }
    */
}
