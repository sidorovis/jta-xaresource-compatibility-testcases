/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.resource.spi.ConnectionManager;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import javax.transaction.xa.XAResource;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import xaresource.compatibility.XAResourceEnlister;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.categories.SpeedSlow;
import xaresource.compatibility.testcases.helpers.XAResourceTestWrapperIgnoreCommitAndRollback;

import com.arjuna.ats.arjuna.common.CoreEnvironmentBean;
import com.arjuna.ats.arjuna.common.CoreEnvironmentBeanException;
import com.arjuna.ats.arjuna.common.arjPropertyManager;
import com.arjuna.ats.arjuna.recovery.RecoveryDriver;
import com.arjuna.ats.arjuna.recovery.RecoveryManager;
import com.arjuna.ats.arjuna.recovery.RecoveryModule;
import com.arjuna.ats.arjuna.tools.RecoveryMonitor;
import com.arjuna.ats.internal.jta.recovery.arjunacore.XARecoveryModule;
import com.arjuna.ats.jta.common.JTAEnvironmentBean;
import com.arjuna.ats.jta.common.jtaPropertyManager;
import com.arjuna.common.internal.util.propertyservice.BeanPopulator;
import com.sun.corba.se.impl.orbutil.closure.Future;

@SuppressWarnings("unused")
@RunWith(Parameterized.class)
@Category(SpeedSlow.class)
public class TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedRecoveryTest {

	private final Logger logger = LoggerFactory.getLogger(TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedRecoveryTest.class);

	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter1;

	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter2;
	
	protected TransactionManager transactionManager;
	
	public static final String nodeName = "myTestNode";
	
	public TwoXAResourcesWithNarayanaTransactionManagerSingleThreadedRecoveryTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1, Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2) {
		this.supplier1 = supplier1;
		this.supplier2 = supplier2;
	}
	
	@Before
	public void setUp() throws CoreEnvironmentBeanException {
		XAResourceEnlister enlister = new XAResourceEnlister(com.arjuna.ats.jta.TransactionManager.transactionManager());
		
		// will enlist the adapter 1 manually
		resourceConnectionFactoryTestAdapter1 = supplier1.apply(new SupplierConstructorArguments());
		resourceConnectionFactoryTestAdapter2 = supplier2.apply(new SupplierConstructorArguments(enlister));

		String nodeName = "myTestNode";
		
		
		CoreEnvironmentBean coreEnvironmentBean = arjPropertyManager.getCoreEnvironmentBean();
		coreEnvironmentBean.setNodeIdentifier(nodeName);
		
		/*
		JTAEnvironmentBean jtaEnvironmentBean = jtaPropertyManager.getJTAEnvironmentBean();
		jtaEnvironmentBean.setXaRecoveryNodes(Arrays.asList(nodeName));
		RecoveryManager.manager().scan();
		*/
				
		transactionManager = com.arjuna.ats.jta.TransactionManager.transactionManager();
	}
	
	// XXX There is still a problem to solve with how the Narayana recovery mechanism works  
	@Test
	public void whenModifyingSingleXAResourcesAndIgnoringCommitFirstAndThenTriggeringRecoveryThenNewValueWillBeVisible() throws InterruptedException, ExecutionException {
		logger.debug("whenModifyingSingleXAResourcesAndIgnoringCommittFirstAndThenTriggeringRecoveryThenNewValueWillBeVisible");
		try {
			UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
			ut.begin();
			
			Transaction tx = transactionManager.getTransaction();
			ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
			tx.enlistResource(new XAResourceTestWrapperIgnoreCommitAndRollback(resourceConnectionTestAdapter1.getXAResource()));
			// XXX the second resource only needs to get enlisted in the transaction so that the TM does not use the onePhase optimization.
			ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
	        
    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
    		resourceConnectionTestAdapter1.doChange1();
    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
    		
	        ut.commit();
	        logger.info("Be aware that the commit is catching the exception and outside of this commit nobody is aware that the transaction failed!!");
    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION); // currently the resource should still be in prepared state!
    		//assertTrue(((xaresource.counter.v2.XAResourceCounterImpl)xaResourceAdapter1.getXAResource()).isLocked());
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");			
		}
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION); // currently the resource should still be in prepared state!
		
		// XXX create another test case where I continue to operate on the same XA resource but this time it does not fail
		//     how do you recover from that?
		//     - Either the XA resource is in such a bad shape that no future transaction will ever work and they all will be silently ignored
		//     - Or the XA resource continues as if the previous transaction simply did not happen at all.
		
		RecoveryManager.manager().initialize();		
		Vector<RecoveryModule> modules = RecoveryManager.manager().getModules();
		for(RecoveryModule module : modules) {
			if(module instanceof XARecoveryModule) {
				XARecoveryModule xaRecoveryModule = (XARecoveryModule) module;
				XAResource[] xaResources = new XAResource[0];
				try {
					xaResources = resourceConnectionFactoryTestAdapter1.getXAResourceRecoveryHelper().getXAResources();
				} catch (Exception e) {
					logger.error("Unexpected exception", e);
					fail("Unexpected exception");
				}
				//assertTrue(((xaresource.counter.v2.XAResourceCounterImpl)(xaResources[0])).isLocked());
				xaRecoveryModule.addXAResourceRecoveryHelper(resourceConnectionFactoryTestAdapter1.getXAResourceRecoveryHelper());
			}
		}
		RecoveryManager.manager().scan();
		RecoveryManager.manager().terminate();

		// the following assertion needs to run on another thread, because on this thread the same transaction may still be visible.
		CompletableFuture<Boolean> verifyChange1InEffect =  new CompletableFuture <Boolean>();
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				verifyChange1InEffect.complete(resourceConnectionTestAdapter1.verifyChange1InEffect());
			}
		});
		t.run();
		assertTrue(verifyChange1InEffect.get());
		/*
		 * normally the thread in which the transaction is running would die. It may not be possible for an xaresource to influence the value of a thread local value from one thread
		 * to another. Therefore the following assertion does not make too much sense:
		 */
		//assertTrue(xaResourceAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		//new Object[]{((Supplier<XAResourceTestAdapter>)TransactionalMapXAResourceTestAdapter::create)},
        		//new Object[]{((Supplier<XAResourceTestAdapter>)XAResourceCounterTestAdapter::create)}
        		new Object[][]{
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create), ((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        				//{(Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create},
        				//{(Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create}
        			}
        		);
    }
    */
}
