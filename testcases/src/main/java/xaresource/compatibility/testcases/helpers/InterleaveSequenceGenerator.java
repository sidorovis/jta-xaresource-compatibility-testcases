/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases.helpers;

import java.util.ArrayList;
import java.util.function.IntFunction;

import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

public class InterleaveSequenceGenerator {

	public static <T> T[][] generate(T[] left, T[] right, IntFunction<T[]> constructor, IntFunction<T[][]> returnConstructor) {
		int totalSize = left.length + right.length;
		Integer[] leftIndices = new Integer[totalSize];
		for(int i=0; i < totalSize; i++) {
			leftIndices[i] = i;
		}
		ICombinatoricsVector<Integer> initialVector = Factory.createVector(leftIndices);
		Generator<Integer> gen = Factory.createSimpleCombinationGenerator(initialVector, left.length);
		ArrayList<T[]> results = new ArrayList<>();
		for (ICombinatoricsVector<Integer> combination : gen) {
			T[] result = constructor.apply(left.length + right.length);
			int j=0; int k=0;
			for(int i = 0; i < totalSize; i++) {
				if(j < left.length && i == combination.getValue(j).intValue()) {
					// take from 'left' array
					result[i] = left[j];
					j++;
				} else {
					result[i] = right[k];
					k++;
				}
			}
			results.add(result);
		}
		
		return results.toArray(returnConstructor.apply(results.size()));
	}
	
}
