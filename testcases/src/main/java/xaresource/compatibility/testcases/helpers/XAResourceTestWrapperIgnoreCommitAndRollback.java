/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases.helpers;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XAResourceTestWrapperIgnoreCommitAndRollback extends XAResourceTestWrapperBase {
	
	private final Logger logger = LoggerFactory.getLogger(XAResourceTestWrapperIgnoreCommitAndRollback.class);	

	public XAResourceTestWrapperIgnoreCommitAndRollback(XAResource wrapped) {
		super(wrapped);
	}

	@Override
	public void commit(Xid xid, boolean onePhase) throws XAException {
		// on purpose ignoring
		String msg = "Ignoring the call to commit: Xid '" + xid + "'";
		logger.info(msg);
		throw new RuntimeException(msg);
	}

	@Override
	public void rollback(Xid xid) throws XAException {
		// on purpose ignoring
		String msg = "Ignoring the call to rollback: Xid '" + xid + "'";
		logger.info(msg);
		throw new RuntimeException(msg);
	}
	
}
