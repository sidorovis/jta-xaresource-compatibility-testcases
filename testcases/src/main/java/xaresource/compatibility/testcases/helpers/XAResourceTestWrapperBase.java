/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases.helpers;

import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XAResourceTestWrapperBase implements XAResource {
	
	private final Logger logger = LoggerFactory.getLogger(XAResourceTestWrapperBase.class);	
	
	public final XAResource wrapped;
	
	public XAResourceTestWrapperBase(XAResource wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public void start(Xid xid, int flags) throws XAException {
		logger.debug("Wrapping call to start() for Xid '" + xid + "'");
		wrapped.start(xid, flags);
	}
	
	@Override
	public void end(Xid xid, int flags) throws XAException {
		logger.debug("Wrapping call to end() for Xid '" + xid + "'");
		wrapped.end(xid, flags);
	}

	@Override
	public int prepare(Xid xid) throws XAException {
		logger.debug("Wrapping call to prepare() for Xid '" + xid + "'");
		return wrapped.prepare(xid);
	}
	
	@Override
	public void commit(Xid xid, boolean onePhase) throws XAException {
		logger.debug("Wrapping call to commit() for Xid '" + xid + "'");
		wrapped.commit(xid, onePhase);
	}

	@Override
	public void rollback(Xid xid) throws XAException {
		logger.debug("Wrapping call to rollback() for Xid '" + xid + "'");
		wrapped.rollback(xid);
	}
	
	@Override
	public void forget(Xid xid) throws XAException {
		logger.debug("Wrapping call to forget() for Xid '" + xid + "'");
		wrapped.forget(xid);
	}

	@Override
	public int getTransactionTimeout() throws XAException {
		logger.debug("Wrapping call to getTransactionTimeout()");
		return wrapped.getTransactionTimeout();
	}

	@Override
	public boolean setTransactionTimeout(int seconds) throws XAException {
		logger.debug("Wrapping call to setTransactionTimeout()");
		return wrapped.setTransactionTimeout(seconds);
	}
	
	@Override
	public boolean isSameRM(XAResource xares) throws XAException {
		logger.debug("Wrapping call to isSameRM()");
		if(xares instanceof XAResourceTestWrapperBase) {
			XAResource xaresWrapped = ((XAResourceTestWrapperBase) xares).wrapped;
			return wrapped.isSameRM(xaresWrapped);
		} else {
			return wrapped.isSameRM(xares);
		}
	}

	@Override
	public Xid[] recover(int flag) throws XAException {
		logger.debug("Wrapping call to recover() for flag '" + flag + "'");
		return wrapped.recover(flag);
	}
}
