/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import javax.transaction.Status;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.categories.SpeedFast;

@RunWith(Parameterized.class)
@Category(SpeedFast.class)
public class SingleXAResourceSinglethreadedTest {
	
	private static final Logger logger = LoggerFactory.getLogger(SingleXAResourceSinglethreadedTest.class);
	
	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter;

	public SingleXAResourceSinglethreadedTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier) {
		this.supplier = supplier;
	}
	
	@Before
	public void setUp() {
		resourceConnectionFactoryTestAdapter = supplier.apply(new SupplierConstructorArguments());
	}
	
	@Test
	public void whenOperatingOnSingleXAResourceAndCommittingThenNewValueWillBeVisible() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		
		// get connection:
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource.end(xid, XAResource.TMSUCCESS);
			int ret = xaresource.prepare(xid);
			if (ret == XAResource.XA_OK) {
				xaresource.commit(xid, false);
			} else {
				fail("Expected to get a 'ret == XAResource.XA_OK'.");				
			}
		} catch (Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}

	@Test
	public void whenOperatingOnSingleXAResourceAndCommittingViaOnePhaseThenNewValueWillBeVisible() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_ACTIVE);
			// do nothing, e.g. read only
			xaresource.end(xid, XAResource.TMSUCCESS);
			int ret = xaresource.prepare(xid);
			if (ret == XAResource.XA_RDONLY) {
				// the work is done!
				assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
				assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
			} else if (ret == XAResource.XA_OK) {
				//fail("Expected to get a 'ret == XAResource.XA_RDONLY'.");				
				xaresource.commit(xid, false);
				assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
				assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
			} else {
				fail("Expected to get a 'ret == XAResource.XA_OK'.");				
			}
		} catch (Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}

	@Test
	public void whenOperatingOnSingleXAResourceAndPerformingReadOnlyWorkThenWorkAfterPrepareIsFinishedAndTransactionCompleted() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource.end(xid, XAResource.TMSUCCESS);
			xaresource.commit(xid, true); // one phase commit
		} catch (Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	@Test
	public void whenOperatingOnSingleXAResourceAndRollingBackThenResourceWillBeBackToOriginalState() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			resourceConnectionTestAdapter.doChange1();
			assertTrue(resourceConnectionTestAdapter.verifyChange1InEffect());
			xaresource.end(xid, XAResource.TMSUCCESS);
			xaresource.prepare(xid);
			xaresource.rollback(xid);
		} catch (XAException e) {
			logger.debug("An XAException was thrown in a rollback, which is not usual, but may happen.", e);
			//fail("No exception is expected here.");
		} /* finally {
			try {
				xaResourceAdapter.close();
			} catch (Exception e) {
				logger.error("Caught and exception while trying to close the xaresource provider.", e);
				fail("No exception is expected here.");
			}
		} */
		assertFalse(resourceConnectionTestAdapter.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
		
	@Test
	public void whenRecoveringPreparedOrHeuristicallyCompletedTransactionBranchesThenTheirEffectShouldNotBeVisible() {
		Xid xid = new MyXid(100, new byte[]{0x01}, new byte[]{0x02});
		Xid[] xids = new Xid[0];
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter = resourceConnectionFactoryTestAdapter.getResourceConnectionTestAdapter();
		XAResource xaresource = resourceConnectionTestAdapter.getXAResource();;
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());

		try {
			xaresource.start(xid, XAResource.TMNOFLAGS);
			resourceConnectionTestAdapter.doChange1();
			xaresource.end(xid, XAResource.TMSUCCESS);
			xaresource.prepare(xid);
		} catch (XAException e) {
			logger.error("XAException was thrown!", e);
			fail("No exception is expected here.");
		}
		
		try {
			xids = xaresource.recover(XAResource.TMSTARTRSCAN | XAResource.TMENDRSCAN);
			List<Xid> xidList = Arrays.asList(xids);
			assertTrue(xidList.size() == 1);
			
			// h2 implements their own JdbcXid and therefore the contains() method does not work!
			Xid theXid = xidList.get(0);
			assertTrue(xid.getFormatId() == theXid.getFormatId());
			assertTrue(Arrays.equals(xid.getGlobalTransactionId(), theXid.getGlobalTransactionId()));
			assertTrue(Arrays.equals(xid.getBranchQualifier(), theXid.getBranchQualifier()));
			//assertTrue(xidList.contains(xid));
		} catch (XAException e) {
			logger.error("Could not get outstanding Xids", e);
		}
		
		for (int i = 0; xids != null && i < xids.length; i++) {
			try {
				xaresource.rollback(xids[i]);
			} catch (XAException ex) {
				fail("No exception is expected here.");
				try {
					xaresource.forget(xids[i]);
				} catch (XAException ex1) {
					logger.error("rollback/forget failed: " + ex1.errorCode, ex1);
				}
			}
		}			
		assertTrue(resourceConnectionTestAdapter.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		//new Object[]{((Supplier<XAResourceTestAdapter>)TransactionalMapXAResourceTestAdapter::create)},
        		//new Object[]{((Supplier<XAResourceTestAdapter>)XAResourceCounterTestAdapter::create)}
        		new Object[][]{
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        				//,{((Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create)}
        			}
        		);
    }
    */
}
