/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import javax.resource.spi.ConnectionManager;

public class SupplierConstructorArguments {
	
	public final ConnectionManager connectionManager;
	public final String idOrConnectionString;
	
	public SupplierConstructorArguments() {
		this.connectionManager = null;
		this.idOrConnectionString = null;
	}

	public SupplierConstructorArguments(String idOrConnectionString) {
		this.connectionManager = null;
		this.idOrConnectionString = idOrConnectionString;
	}

	public SupplierConstructorArguments(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
		this.idOrConnectionString = null;
	}
	
	public SupplierConstructorArguments(String idOrConnectionString, ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
		this.idOrConnectionString = idOrConnectionString;
	}
	
	public TransientSupplierConstructorArguments builder() {
		return new TransientSupplierConstructorArguments(this);
	}
	
	public static class TransientSupplierConstructorArguments {
		public ConnectionManager connectionManager = null;
		public String idOrConnectionString = null;
		
		public TransientSupplierConstructorArguments(SupplierConstructorArguments sca) {
			this.connectionManager = sca.connectionManager;
			this.idOrConnectionString = sca.idOrConnectionString;
		}
		
		public TransientSupplierConstructorArguments setConnectionManager(ConnectionManager connectionManager) {
			this.connectionManager = connectionManager;
			return this;
		}
		
		public TransientSupplierConstructorArguments setIdOrConnectionString(String idOrConnectionString) {
			this.idOrConnectionString = idOrConnectionString;
			return this;
		}
		
		public SupplierConstructorArguments seal() {
			return new SupplierConstructorArguments(this.idOrConnectionString, this.connectionManager);
		}
	}
}
