/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.resource.spi.ConnectionManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.UserTransaction;
import javax.transaction.xa.XAResource;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.CallbackPreferringPlatformTransactionManager;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionOperations;
import org.springframework.transaction.support.TransactionTemplate;

import xaresource.compatibility.IsolationLevel;
import xaresource.compatibility.XAResourceEnlister;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter.InvariantPartner;
import xaresource.compatibility.testcases.categories.SpeedSlow;

import com.arjuna.ats.arjuna.common.CoreEnvironmentBean;
import com.arjuna.ats.arjuna.common.CoreEnvironmentBeanException;
import com.arjuna.ats.arjuna.common.arjPropertyManager;
import com.arjuna.ats.arjuna.recovery.RecoveryDriver;
import com.arjuna.ats.arjuna.recovery.RecoveryManager;
import com.arjuna.ats.arjuna.recovery.RecoveryModule;
import com.arjuna.ats.arjuna.tools.RecoveryMonitor;
import com.arjuna.ats.internal.jta.recovery.arjunacore.XARecoveryModule;
import com.arjuna.ats.jta.common.JTAEnvironmentBean;
import com.arjuna.ats.jta.common.jtaPropertyManager;
import com.arjuna.common.internal.util.propertyservice.BeanPopulator;


/*
 * http://stackoverflow.com/questions/2235276/how-to-run-junit-test-cases-from-the-command-line
 * 
 * in order to run this multi threaded test case outside of eclipse and or maven directly on the command line do the following:
 * - Adapt the pom.xml file <mvnTestScope> to 'compile' instead of 'test'. Don't forget to change that back afterwards again.
 * - Perform a 'mvn package'
 * - Run the test case from the command line via the following command:
 *   java -cp target/test-classes:target/classes:target/xa-resource-counter-1.0-SNAPSHOT-jar-with-dependencies.jar org.junit.runner.JUnitCore xaresource.TwoXAResourcesWithNarayanaTransactionManagerMultiThreadedInvariantTest
 */

@SuppressWarnings("unused")
@RunWith(Parameterized.class)
@Category(SpeedSlow.class) // will run in less than 30 seconds if you set the log level to info or above; otherwise the logging to the console is the problem.
public class TwoXAResourcesWithNarayanaTransactionManagerMultiThreadedInvariantTest {

	private static final Logger logger = LoggerFactory.getLogger(TwoXAResourcesWithNarayanaTransactionManagerMultiThreadedInvariantTest.class);
	
	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter1;

	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter2;
	
	protected TransactionManager transactionManager;
	
	protected AtomicBoolean invariantViolated = new AtomicBoolean(false);
	protected static final int iterationCount = 50;
	protected static final int threadCount = 2;
	protected AtomicInteger successfulCommitCount = new AtomicInteger(0);
	protected AtomicInteger rollbackDueToUnsuccessfulCommitCount = new AtomicInteger(0);

	protected AtomicInteger successfulVerificationCount = new AtomicInteger(0);
	protected AtomicInteger rollbackVerificationCount = new AtomicInteger(0);
	protected XAResourceEnlister enlister = null;
	
	public static final String nodeName = "myTestNode";
	
	public TwoXAResourcesWithNarayanaTransactionManagerMultiThreadedInvariantTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1, Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2) {
		this.supplier1 = supplier1;
		this.supplier2 = supplier2;
	}
	
	@Before
	public void setUp() throws CoreEnvironmentBeanException {
		enlister = new XAResourceEnlister(com.arjuna.ats.jta.TransactionManager.transactionManager());
		
		resourceConnectionFactoryTestAdapter1 = supplier1.apply(new SupplierConstructorArguments(enlister));
		resourceConnectionFactoryTestAdapter2 = supplier2.apply(new SupplierConstructorArguments(enlister));
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		resourceConnectionTestAdapter1.invariantTestInit(InvariantPartner.one);
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		resourceConnectionTestAdapter2.invariantTestInit(InvariantPartner.two);

		String nodeName = "myTestNode";
		
		
		CoreEnvironmentBean coreEnvironmentBean = arjPropertyManager.getCoreEnvironmentBean();
		coreEnvironmentBean.setNodeIdentifier(nodeName);
		
		transactionManager = com.arjuna.ats.jta.TransactionManager.transactionManager();
	}
	
	@After
	public void tearDown() throws Exception {
		//xaResourceAdapter1.close();
		//xaResourceAdapter2.close();
	}
	
	public void doWork1() {
		logger.debug("Entering doWork1()");
		
		IsolationLevel l1 = resourceConnectionFactoryTestAdapter1.getIsloationLevel();
		IsolationLevel l2 = resourceConnectionFactoryTestAdapter2.getIsloationLevel();
		boolean isSerializableRead = (l1 == IsolationLevel.Serializable) && (l2 == IsolationLevel.Serializable);
		
		int j = 0;
		try {
			for(j = 0; j < iterationCount; j++) {
				final int i = j;
				logger.debug("doWork(): i=" + i);
				try {
					UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
					ut.begin();
			        try {
						ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
						ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        
			        	// this strange construct of first checking the condition and then asserting it is in order to be able to set breakpoints on it
				        if(!(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION))
				        	assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
				        resourceConnectionTestAdapter1.invariantTestMinus();
						assertTrue("Block 1: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
				        if(!(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION))
				        	assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
				        resourceConnectionTestAdapter2.invariantTestPlus();
						assertTrue("Block 1: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());			        	
			        } catch(Throwable e) {
			        	logger.info("Block 1: Caught a runtime exception and setting rollbackonly:" + i, e);
			        	ut.setRollbackOnly();
			        } finally {
				        ut.commit();			        	
			        }
			        successfulCommitCount.incrementAndGet();
				} catch(RollbackException e) {
					logger.info("Block 1: RollbackException was thrown!" + i, e);
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				} catch(HeuristicMixedException | HeuristicRollbackException e) {
					logger.info("Block 1: HeuristicMixedException was thrown!" + i, e);
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				} catch(Exception e) {
					logger.info("Block 1: Exception was thrown!" + i, e);
					fail("Block 1: No exception is expected here." + i);			
				}
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 1));
				
				try {
					UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
					ut.begin();
			        try {
						ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
						ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        
				        if(!(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION))
				        	assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
				        resourceConnectionTestAdapter2.invariantTestMinus();
						assertTrue("Block 2: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
				        if(!(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION))
				        	assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
				        resourceConnectionTestAdapter1.invariantTestPlus();
						assertTrue("Block 2: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
			        } catch(Throwable e) {
			        	logger.info("Block 2: Caught a runtime exception and setting rollbackonly:" + i, e);
			        	ut.setRollbackOnly();
			        } finally {
				        ut.commit();
			        }
			        successfulCommitCount.incrementAndGet();
				} catch(RollbackException e) {
					logger.info("Block 2: RollbackException was thrown!" + i, e);
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				} catch(HeuristicMixedException | HeuristicRollbackException e) {
					logger.info("Block 2: HeuristicMixedException was thrown!" + i, e);
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				} catch(Exception e) {
					logger.info("Block 2: Exception was thrown!" + i, e);
					fail("Block 2: No exception is expected here." + i);			
				}
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 2));

				try {
					UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
					ut.begin();
			        try {
						ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
						ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        
				        if(!(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION))
				        	assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
				        resourceConnectionTestAdapter2.invariantTestRead();
						assertTrue("Block 3: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
				        if(!(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION))
				        	assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
				        resourceConnectionTestAdapter1.invariantTestRead();
						assertTrue("Block 3: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
			        } catch(Throwable e) {
			        	logger.info("Block 3: Caught a runtime exception and setting rollbackonly:" + i, e);
			        	ut.setRollbackOnly();
			        } finally {
				        ut.commit();			        	
			        }
			        successfulCommitCount.incrementAndGet();
				} catch(RollbackException e) {
					logger.info("Block 3: RollbackException was thrown!" + i, e);
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				} catch(HeuristicMixedException | HeuristicRollbackException e) {
					logger.info("Block 3: HeuristicMixedException was thrown!" + i, e);
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				} catch(Exception e) {
					logger.info("Block 3: Exception was thrown!" + i, e);
					fail("Block 3: No exception is expected here." + i);			
				}
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 3));
				
				// -- now looking into rollbacks
				
				try {
					UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
					ut.begin();
			        try {
						ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
						ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        
			    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			    		resourceConnectionTestAdapter1.invariantTestMinus();
						assertTrue("Block 4: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
			    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
			    		resourceConnectionTestAdapter2.invariantTestPlus();
						assertTrue("Block 4: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
			        } catch(Throwable e) {
			        	logger.info("Block 4: Caught a runtime exception and setting rollbackonly:" + i, e);
			        	ut.setRollbackOnly();
			        } finally {
				        ut.rollback();			        	
			        }
				} catch(Exception e) {
					logger.info("Block 4: Exception was thrown!" + i, e);
					fail("Block 4: No exception is expected here." + i);			
				}
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 4));

				try {
					UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
					ut.begin();
			        try {
						ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
						ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        
			    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
			    		resourceConnectionTestAdapter2.invariantTestMinus();
						assertTrue("Block 5: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
			    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			    		resourceConnectionTestAdapter1.invariantTestPlus();
						assertTrue("Block 5: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
			        } catch(Throwable e) {
			        	logger.info("Block 5: Caught a runtime exception and setting rollbackonly:" + i, e);
			        	ut.setRollbackOnly();
			        } finally {
				        ut.rollback();			        	
			        }
				} catch(Exception e) {
					logger.info("Block 5: Exception was thrown!" + i, e);
					fail("Block 5: No exception is expected here." + i);			
				}
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 5));

				try {
					UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
					ut.begin();
			        try {
						ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
						ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        
			    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
			    		resourceConnectionTestAdapter2.invariantTestRead();;
						assertTrue("Block 6: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
			    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			    		resourceConnectionTestAdapter1.invariantTestRead();
						assertTrue("Block 6: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
			        } catch(Throwable e) {
			        	logger.info("Block 6: Caught a runtime exception and setting rollbackonly:" + i, e);
			        	ut.setRollbackOnly();
			        } finally {
				        ut.rollback();			        	
			        }
				} catch(Exception e) {
					logger.info("Block 6: Exception was thrown!" + i, e);
					fail("Block 6: No exception is expected here." + i);			
				}
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 6));
				
				if(invariantViolated.get()) {
					logger.error("Whole Loop: Detected an invariantViolated condition and breaking out of loop:" + i);
					break;
				}
			}
		} catch(Throwable e) {
			invariantViolated.set(true);
			logger.error("Invariant violated: " + j, e);
		} /* finally {
			try {
				xaResourceAdapter1.close();
			} catch (Exception e) {
				logger.error("Exception while trying to close the counter!" + j, e);
			}
			try {
				xaResourceAdapter2.close();
			} catch (Exception e) {
				logger.error("Exception while trying to close the counter!" + j, e);
			}
			logger.debug("Exiting doWork() + i");			
		}*/
	}

	public void doWork2() {
		logger.debug("Entering doWork()");
		
		IsolationLevel l1 = resourceConnectionFactoryTestAdapter1.getIsloationLevel();
		IsolationLevel l2 = resourceConnectionFactoryTestAdapter2.getIsloationLevel();
		boolean isSerializableRead = (l1 == IsolationLevel.Serializable) && (l2 == IsolationLevel.Serializable);
		
		UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
		LocalTransactionTemplate template =  new LocalTransactionTemplate(ut, transactionManager);
		
		int j = 0;
		try {
			for(j = 0; j < iterationCount; j++) {
				final int i = j;
				logger.info("doWork(): i=" + i);
				template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
					ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
					ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		        	// this strange construct of first checking the condition and then asserting it is in order to be able to set breakpoints on it
			        if(!(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION))
			        	assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			        resourceConnectionTestAdapter1.invariantTestMinus();
					assertTrue("Block 1: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
			        if(!(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION))
			        	assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
			        resourceConnectionTestAdapter2.invariantTestPlus();
					assertTrue("Block 1: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
					return null;
				}, i, 1, true);
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 1));
				
				template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
					ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
					ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        if(!(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION))
			        	assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
			        resourceConnectionTestAdapter2.invariantTestMinus();
					assertTrue("Block 2: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
			        if(!(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION))
			        	assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			        resourceConnectionTestAdapter1.invariantTestPlus();
					assertTrue("Block 2: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
					return null;
				}, i, 2, true);
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 2));

				template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
					ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
					ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			        if(!(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION))
			        	assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
			        resourceConnectionTestAdapter2.invariantTestRead();
					assertTrue("Block 3: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
			        if(!(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION))
			        	assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
			        resourceConnectionTestAdapter1.invariantTestRead();
					assertTrue("Block 3: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
					return null;
				}, i, 3, true);
				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 3));
				
				// -- now looking into rollbacks
				
				template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
					ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
					ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
		    		resourceConnectionTestAdapter1.invariantTestMinus();
					assertTrue("Block 4: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
		    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
		    		resourceConnectionTestAdapter2.invariantTestPlus();
					assertTrue("Block 4: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
					status.setRollbackOnly();
					return null;
				}, i, 4, false);

				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 4));

				template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
					ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
					ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
		    		resourceConnectionTestAdapter2.invariantTestMinus();
					assertTrue("Block 5: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
		    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
		    		resourceConnectionTestAdapter1.invariantTestPlus();
					assertTrue("Block 5: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
					status.setRollbackOnly();
					return null;
				}, i, 5, false);

				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 5));

				template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
					ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
					ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		    		assertTrue(resourceConnectionTestAdapter2.getStatus() != Status.STATUS_NO_TRANSACTION);
		    		resourceConnectionTestAdapter2.invariantTestRead();;
					assertTrue("Block 6: xaResourceAdapter2.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter2.invariantTestVerifyChangeInEffect());
		    		assertTrue(resourceConnectionTestAdapter1.getStatus() != Status.STATUS_NO_TRANSACTION);
		    		resourceConnectionTestAdapter1.invariantTestRead();
					assertTrue("Block 6: xaResourceAdapter1.invariantTestVerifyChangeInEffect()" +  i, resourceConnectionTestAdapter1.invariantTestVerifyChangeInEffect());
					status.setRollbackOnly();
					return null;
				}, i, 6, false);

				if(isSerializableRead)
					assertTrue("transactionalReadVerifyOfInvariant(): " + i, transactionalReadVerifyOfInvariant(i, 6));
				
				if(invariantViolated.get()) {
					logger.error("Whole Loop: Detected an invariantViolated condition and breaking out of loop:" + i);
					break;
				}
			}
		} catch(Throwable e) {
			invariantViolated.set(true);
			logger.error("Invariant violated: " + j, e);
		} /* finally {
			try {
				xaResourceAdapter1.close();
			} catch (Exception e) {
				logger.error("Exception while trying to close the counter!" + j, e);
			}
			try {
				xaResourceAdapter2.close();
			} catch (Exception e) {
				logger.error("Exception while trying to close the counter!" + j, e);
			}
			logger.debug("Exiting doWork() + i");			
		}*/
	}
	
	/*
	public boolean transactionalReadVerifyOfInvariant(LocalTransactionTemplate template, int i, int block) {
		ResourceConnectionFactoryTestAdapter writeToInstance = resourceConnectionFactoryTestAdapter1.createFreshWriteToInstance(enlister);
		template.execute((TransactionCallback<Void>)(TransactionStatus status) -> {
			ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
			ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
			resourceConnectionTestAdapter1.invariantTestVerifyInTransaction(resourceConnectionTestAdapter2, writeToInstance.getResourceConnectionTestAdapter());
	        return null;
		}, i, block, false);
		
        successfulVerificationCount.incrementAndGet();
        boolean r = writeToInstance.getResourceConnectionTestAdapter().invariantTestVerifyAfterTransactionOnWriteToInstance();
        if(r == false) {
        	logger.error("Block " + block + ": The invariantTestVerifyAfterTransaction() returned false: " + i, new RuntimeException("Stack Trace"));
        }
		
		return r;
	}
	*/
	
	public boolean transactionalReadVerifyOfInvariant(int i, int block) {
		ResourceConnectionFactoryTestAdapter writeToInstance = resourceConnectionFactoryTestAdapter1.createFreshWriteToInstance(enlister);
		try {
			UserTransaction ut = com.arjuna.ats.jta.UserTransaction.userTransaction();
			ut.begin();
	        try {
				ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
				ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
	        	
				resourceConnectionTestAdapter1.invariantTestVerifyInTransaction(resourceConnectionTestAdapter2, writeToInstance.getResourceConnectionTestAdapter());
	        } catch(RuntimeException e) {
	        	logger.info("Block " + block + ": Caught a runtime exception and setting rollbackonly:" + i, e);
	        	ut.setRollbackOnly();
	        } finally {
		        ut.commit();
	        }
	        successfulVerificationCount.incrementAndGet();
	        return writeToInstance.getResourceConnectionTestAdapter().invariantTestVerifyAfterTransactionOnWriteToInstance();
		}  catch(RollbackException e) {
			// fine; we just had bad luck to verify this test case
			rollbackVerificationCount.incrementAndGet();
		}  catch(Exception e) {
			logger.info("Block " + block + ": Exception was thrown in transactionalReadVerifyOfInvariant()!" + i, e);
			fail("Block " + block + ": No exception is expected here." + i);
		}
		return true;
	}
	
	@Test
	public void invariantTest() throws InterruptedException {
		Thread[] threads = new Thread[threadCount];
		for(int i = 0; i < threadCount; i++) {
			threads[i] =  new Thread(() -> doWork2());
			threads[i].start();
		}
		
		for(int i = 0; i < threadCount; i++) {
			threads[i].join();
		}
		
		logger.info("successfulCommitCount: " + successfulCommitCount.get());
		logger.info("rollbackDueToUnsuccessfulCommitCount: " + rollbackDueToUnsuccessfulCommitCount.get());
		
		logger.info("successfulVerificationCount: " + successfulVerificationCount.get());
		logger.info("rollbackVerificationCount: " + rollbackVerificationCount.get());
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.invariantTestVerify(resourceConnectionTestAdapter2));
		
		assertFalse(invariantViolated.get());
		
		assertThat("The number of successful commits and unsuccessful rollbacks should be equal to the number of such attempts.", 
				successfulCommitCount.get() + rollbackDueToUnsuccessfulCommitCount.get(), 
				is(3 * threadCount * iterationCount));
	}
	
	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		//new Object[]{((Supplier<XAResourceTestAdapter>)TransactionalMapXAResourceTestAdapter::create)},
        		//new Object[]{((Supplier<XAResourceTestAdapter>)XAResourceCounterTestAdapter::create)}
        		new Object[][]{
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create), ((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        				//{(Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create}
        				//,{(Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create}
        			}
        		);
    }
    */
    
    public class LocalTransactionTemplate extends TransactionTemplate {
    	
		private static final long serialVersionUID = 1L;

		private final Logger logger = LoggerFactory.getLogger(LocalTransactionTemplate.class);
    	
		protected final JtaTransactionManager springJtaTransactionManager;

		public LocalTransactionTemplate(UserTransaction userTransaction, TransactionManager transactionManager) {
			super(new JtaTransactionManager(userTransaction, transactionManager));
    		this.springJtaTransactionManager = (JtaTransactionManager) this.getTransactionManager();
    	}
    	
		public <T> T execute(TransactionCallback<T> action, int i, int block, boolean count) throws TransactionException {
			try {
				if (this.getTransactionManager() instanceof CallbackPreferringPlatformTransactionManager) {
					return ((CallbackPreferringPlatformTransactionManager) this.getTransactionManager()).execute(this, action);
				}
				else {
					TransactionStatus status = this.getTransactionManager().getTransaction(this);
					T result;
					try {
						Transaction tx = springJtaTransactionManager.getTransactionManager().getTransaction();
						result = action.doInTransaction(status);
					}
					catch (RuntimeException ex) {
						// Transactional code threw application exception -> rollback
						rollbackOnException(status, ex, i, block);
						throw ex;
					}
					catch (Error err) {
						// Transactional code threw error -> rollback
						rollbackOnException(status, err, i, block);
						throw err;
					}
					catch (Exception ex) {
						// Transactional code threw unexpected exception -> rollback
						rollbackOnException(status, ex, i, block);
						throw new UndeclaredThrowableException(ex, "TransactionCallback threw undeclared checked exception");
					}
					this.getTransactionManager().commit(status);
					if(count) {
						if(status.isRollbackOnly())
							rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
						else
							successfulCommitCount.incrementAndGet();					
					}
					return result;
				}				
			} catch(Throwable t) {
				if(count)
					rollbackDueToUnsuccessfulCommitCount.incrementAndGet();
				logger.info("Block " + block + ": Exception was thrown! " + i, t);
			}
			return null;
		}
		
		/**
		 * Perform a rollback, handling rollback exceptions properly.
		 * @param status object representing the transaction
		 * @param ex the thrown application exception or error
		 * @throws TransactionException in case of a rollback error
		 */
		private void rollbackOnException(TransactionStatus status, Throwable ex, int i, int block) throws TransactionException {
			logger.debug("Initiating transaction rollback on application exception", ex);
			try {
				this.getTransactionManager().rollback(status);
			}
			catch (TransactionSystemException ex2) {
				logger.error("Application exception overridden by rollback exception", ex);
				ex2.initApplicationException(ex);
				throw ex2;
			}
			catch (RuntimeException ex2) {
				logger.error("Application exception overridden by rollback exception", ex);
				throw ex2;
			}
			catch (Error err) {
				logger.error("Application exception overridden by rollback error", ex);
				throw err;
			}
		}
		
		public JtaTransactionManager getJtaTransactionManager() {
			return springJtaTransactionManager;
		}
		
    }
}
