/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.inject.Inject;
import javax.resource.spi.ConnectionManager;
import javax.transaction.Status;
import javax.transaction.Transaction;
import javax.transaction.UserTransaction;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.jta.JtaTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationManager;


import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContextManager;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import xaresource.compatibility.XAResourceEnlister;
import xaresource.compatibility.testcases.adapters.ResourceConnectionFactoryTestAdapter;
import xaresource.compatibility.testcases.adapters.ResourceConnectionTestAdapter;
import xaresource.compatibility.testcases.categories.SpeedFast;

/*
 * http://spring.io/blog/2011/08/15/configuring-spring-and-jta-without-full-java-ee/
 * http://docs.spring.io/spring/docs/4.0.6.RELEASE/spring-framework-reference/htmlsingle/#transaction
 * http://www.javaworld.com/article/2077714/java-web-development/xa-transactions-using-spring.html
 * http://doctorjw.wordpress.com/2013/06/26/xa-transactions-with-spring-and-hibernate/
 */

// http://www.blog.project13.pl/index.php/coding/1077/runwith-junit4-with-both-springjunit4classrunner-and-parameterized/
@SuppressWarnings("unused")
@ContextConfiguration("/xa-distributed-transaction-spring-declarative-transaction-test-suite-config.xml")
@TestExecutionListeners(listeners = {DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class})
@RunWith(Parameterized.class)
@Category(SpeedFast.class)
public class TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTest {
	
	private final Logger logger = LoggerFactory.getLogger(TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTest.class);
	
	private TestContextManager testContextManager;
	
	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter1;

	protected final Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2;
	protected ResourceConnectionFactoryTestAdapter resourceConnectionFactoryTestAdapter2;
	
	public TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTest(Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier1, Function<@NonNull SupplierConstructorArguments, ResourceConnectionFactoryTestAdapter> supplier2) {
		this.supplier1 = supplier1;
		this.supplier2 = supplier2;
	}
	
    @Inject // works, too
    private TwoXAResourcesWithNarayanaTransactionManagerAndSpringDeclarativeTransactionSingleThreadedTestDoWork doWork;
	
	@Before
	public void setUp() throws Exception {
		// the following two lines replace the @RunWith(SpringJUnit4ClassRunner.class) annotation's effect:
        this.testContextManager = new TestContextManager(getClass());
        this.testContextManager.prepareTestInstance(this);
		
        
		XAResourceEnlister enlister = new XAResourceEnlister(com.arjuna.ats.jta.TransactionManager.transactionManager());
		
		resourceConnectionFactoryTestAdapter1 = supplier1.apply(new SupplierConstructorArguments(enlister));
		resourceConnectionFactoryTestAdapter2 = supplier2.apply(new SupplierConstructorArguments(enlister));
	}
	
	@Test
	public void whenModifyingTwoIndependentXAResourcesAndCommittingBothNewValuesWillBeVisible() {
		// references so that you can quickly jump to these classes in order to set breakpoints:
		com.arjuna.ats.internal.jta.transaction.arjunacore.UserTransactionImple ut = null;
		com.arjuna.ats.internal.jta.transaction.arjunacore.TransactionManagerImple tm = null;
		com.arjuna.ats.internal.jta.transaction.arjunacore.BaseTransaction bt = null;
		try {
			doWork.doWork(resourceConnectionFactoryTestAdapter1, resourceConnectionFactoryTestAdapter2);
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");			
		}
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter2.verifyChange1InEffect());
		assertTrue(resourceConnectionTestAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
		assertTrue(resourceConnectionTestAdapter2.getStatus() == Status.STATUS_NO_TRANSACTION);
	}
	
	@Test
	public void whenModifyingTwoIndependentXAResourcesAndRollingBackThenNoneOfTheChangesWillBeVisible() {
		try {
			doWork.failWork(resourceConnectionFactoryTestAdapter1, resourceConnectionFactoryTestAdapter2);
			fail("Expecting a Runtime Exception that will roll back the transaction.");			
		} catch(RuntimeException e) {
			// All fine; as expected
		} catch(Exception e) {
			logger.error("Exception was thrown!", e);
			fail("No exception is expected here.");
		}
		
		ResourceConnectionTestAdapter resourceConnectionTestAdapter1 = resourceConnectionFactoryTestAdapter1.getResourceConnectionTestAdapter();
		ResourceConnectionTestAdapter resourceConnectionTestAdapter2 = resourceConnectionFactoryTestAdapter2.getResourceConnectionTestAdapter();
		assertTrue(resourceConnectionTestAdapter1.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter2.verifyResourceInInitialState());
		assertTrue(resourceConnectionTestAdapter1.getStatus() == Status.STATUS_NO_TRANSACTION);
		assertTrue(resourceConnectionTestAdapter2.getStatus() == Status.STATUS_NO_TRANSACTION);
	}

	/*
    @Parameterized.Parameters
    public static Collection<Object[]> instancesToTest() {
        return Arrays.asList(
        		new Object[][]{
        				//{(Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceDbCounterTestAdapter::create}, 
        				//{(Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create, (Supplier<XAResourceTestAdapter>)XAResourceV3CounterTestAdapter::create}
        				{((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create), ((Function<ConnectionManager, ResourceConnectionFactoryTestAdapter>)V4CounterResourceConnectionFactoryTestAdapter::create)}
        			}
        		);
    }
    */
}
