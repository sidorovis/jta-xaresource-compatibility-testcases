/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases.adapters;

import javax.resource.spi.ConnectionManager;

import xaresource.compatibility.IsolationLevel;

import com.arjuna.ats.jta.recovery.XAResourceRecoveryHelper;

public interface ResourceConnectionFactoryTestAdapter {
	
	public ResourceConnectionTestAdapter getResourceConnectionTestAdapter();
	
	public static enum TransactionStartsWith {START, CHANGE};
	public IsolationLevel getIsloationLevel();
	public TransactionStartsWith getTransactionStartsWith();
	public boolean isChange2Independent();
	public boolean isPessimisticLocking();

	public XAResourceRecoveryHelper getXAResourceRecoveryHelper();
	
	public ResourceConnectionFactoryTestAdapter createFreshWriteToInstance(ConnectionManager intercept);
}
