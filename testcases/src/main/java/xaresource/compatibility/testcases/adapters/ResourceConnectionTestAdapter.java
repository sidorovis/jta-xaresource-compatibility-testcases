/**
 * Copyright (c) 2014 Christian Schuhegger. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file LICENSE.md at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 *
 */
package xaresource.compatibility.testcases.adapters;

import javax.transaction.xa.XAResource;

public interface ResourceConnectionTestAdapter {

	public XAResource getXAResource();
	public int getStatus();
	
	// in transaction changes:
	public boolean verifyResourceInInitialState();
	public void doChange1();
	public boolean verifyChange1InEffect();
	public void doChange2();
	public boolean verifyChange2InEffect();
	public void doChange3();
	public boolean verifyChange3InEffect();
	
	public enum InvariantPartner {one, two};
	public void invariantTestInit(InvariantPartner invariantPartner);
	public void invariantTestMinus();
	public void invariantTestPlus();
	public void invariantTestRead();
	public boolean invariantTestVerifyChangeInEffect();
	public boolean invariantTestVerify(ResourceConnectionTestAdapter other);
	
	public void invariantTestVerifyInTransaction(ResourceConnectionTestAdapter other, ResourceConnectionTestAdapter writeToInstance);
	public boolean invariantTestVerifyAfterTransactionOnWriteToInstance();
	
	
}
