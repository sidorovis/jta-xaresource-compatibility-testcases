# README #

### What is this repository for? ###

* This repository contains testcases that guide you in the implementation of Java Transaction API (JTA) compatible implementations of XAResource instances.
* 0.01
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Start by creating the project workspace directory, by cloning the repository:

	> git clone https://bitbucket.org/jta-xaresource-compatibility/jta-xaresource-compatibility-testcases.git

Change the PWD to this directory:

	> cd jta-xaresource-compatibility-testcases

And initialize the directory as a Eclipse workspace:

	> mvn eclipse:configure-workspace -Declipse.workspace=./

Now you can initialize all sub-projects as Eclipse projects:

	> mvn eclipse:eclipse

Then open Eclipse with the project workspace directory:

	> cd ../
	> eclipse -data jta-xaresource-compatibility-testcases

And finally import the projects into Eclipse via:

	>File>Import>General>Existing Projects Into Workspace
	
Click 'Browse' and then 'Ok' and select the jta-xaresource-compatibility-testcases project.

Finally you should have everything set-up to start working on the project in Eclipse.

### Contribution guidelines ###

The [Collective Code Construction Contract](http://rfc.zeromq.org/spec:16) (C4) is an evolution of the github.com Fork + Pull Model, aimed at providing an optimal collaboration model for free software projects. C4 is derived from the ZeroMQ contribution policy of early 2012.

### Who do I talk to? ###

* This repository is a team repository. Talk to one of its admins if you have any questions you would like to ask.